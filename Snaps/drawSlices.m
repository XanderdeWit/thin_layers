function h=drawSlices(xs,ys,zs,xslice,yslice,zslice,values,valueLim,cm,axisImage)

[x3d,y3d,z3d]=meshgrid(xs,ys,zs);
values_3d=permute(values,[2 1 3]);

h=slice(x3d,y3d,z3d,values_3d,xslice,yslice,zslice);
shading interp
colormap(cm)
colorbar('TickLabelInterpreter','latex')
caxis(valueLim)
xlabel('$x$')
ylabel('$y$')
zlabel('$z$')
set(get(gca,'ZLabel'),'Rotation',0,'VerticalAlignment','middle','HorizontalAlignment','right')

set(get(gca,'xlabel'),'position',[xs(end)/2,-0.12,0])
set(get(gca,'ylabel'),'position',[-0.12,ys(end)/2,0])

if axisImage
    axis image
end

end

