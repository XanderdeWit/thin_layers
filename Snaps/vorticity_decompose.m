snap_dir='snaps/Nu1e-3_nxz512_ny16_Qy32/';
%snap_dir='snaps/Redouble/OUT/';

snap_vx='hd3Dvz.stitch.003.mat';
snap_vy='hd3Dvx.stitch.003.mat';
%snap_vz='hd3Dvy.stitch.003.mat';

%Qy=32;
Qy=12; %use different Qy for visual purposes

load([gDataDir snap_dir snap_vx]); vx=data;
load([gDataDir snap_dir snap_vy]); vy=data;
%load([gDataDir snap_dir snap_vz]); vz=data;

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

%total
data=getAxialVorticity(vx,vy,2*pi/size(data,1),2*pi/size(data,2));

valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')

%2D K=1
vx_2d=mean(vx,3); vy_2d=mean(vy,3);

%fourier filter
vx_2d_k=fft2(vx_2d);%./(size(data,1)*size(data,2));
vy_2d_k=fft2(vy_2d);%./(size(data,1)*size(data,2));
kmax=1; %filter out modes larger than kmax
kx=[0:size(data,1)/2,-size(data,1)/2+1:-1];
ky=[0:size(data,2)/2,-size(data,2)/2+1:-1];
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mk=floor(sqrt(mkx^2+mky^2));
        if mk>kmax %discard modes larger than kmax
            vx_2d_k(ikx,iky)=0;
            vy_2d_k(ikx,iky)=0;
        end
    end
end
vx_ls=ifft2(vx_2d_k);
vy_ls=ifft2(vy_2d_k);

vx_ls=repmat(vx_ls,1,1,size(data,3));
vy_ls=repmat(vy_ls,1,1,size(data,3));

data=getAxialVorticity(vx_ls,vy_ls,2*pi/size(data,1),2*pi/size(data,2));

%valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')

%other
vx_nls=vx-vx_ls; vy_nls=vx-vx_ls;

data=getAxialVorticity(vx_nls,vy_nls,2*pi/size(data,1),2*pi/size(data,2));

%valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')