moviedir='movies/testrun_movie5/';

Qy=16.0;

dtime=0.2;

%Large scale energy timeseries
tsEls=getTsFromFile([gDataDir moviedir 'energy2D_ls_balance.txt'],7,1,2,true);
timescale=getScale('time',0.5,2*pi/8.0);
energyscale=getScale('energy',0.5,2*pi/8.0);

file = fopen([gDataDir moviedir 'field_times.txt'],'r');
field_times = fscanf(file,'%f %f\n',[2 Inf]);

%sort the snaps
if any(diff(field_times(1,:))<0)
    pad1=-min(diff(field_times(1,:)))+1;
    shift=find(field_times(1,:)==pad1+1,1,'last')-find(field_times(1,:)==1,1,'last');
    n=zeros(1,field_times(1,end));
    n(1:pad1-shift)=shift+1:pad1;
    n(pad1-shift+1:pad1)=1:shift;
    n(pad1+1:end)=pad1+1:field_times(1,end);
else
    n=field_times(1,:);
end

snap=['hd3Dwy.stitch.' num2str(n(1),'%03d') '.mat'];

load([gDataDir moviedir snap])

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

valueLim=0.8*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

h=figure;
figSize=[800 600];
set(gcf, 'Position',  [100, 100, figSize(1), figSize(2)])
axis tight manual % this ensures that getframe() returns a consistent size
filename = 'testAnimated.gif';

ax1=gca;

for in=1:length(n)
    time=field_times(2,find(field_times(1,:)==n(in),1,'last'));
    if in==1
        ax2=axes('Position',[.75 .1 .2 .2]);
        plot(tsEls.Time/timescale,tsEls.Data/energyscale)
        xlabel('$t/\tau_f$')
        ylabel('$E_{ls}/E_f$')
        box on
        l=xline(time,'--','Color',gPlotColors(2,:),'LineWidth',2);
    else
        l.Value=time/timescale;
    end
    
    snap=['hd3Dwy.stitch.' num2str(n(in),'%03d') '.mat'];
    load([gDataDir moviedir snap])
    
    if in ==1
        [x3d,y3d,z3d]=meshgrid(xs,ys,zs);
        axes(ax1)
    end
    
    values_3d=permute(-data,[2 1 3]);
    slice(x3d,y3d,z3d,values_3d,0,0,zs(end));
    shading interp
    
    if in==1
        colormap(gMyCM)
        %colorbar('TickLabelInterpreter','latex')
    end
    
    caxis(valueLim)
    axis image
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    set(gca,'ztick',[])
    hold on
    line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
    line([0,0],ylim,[zs(end) zs(end)],'Color','k')
    line([0,0],[0,0],[0,zs(end)],'Color','k')
    hold off
    box on
    
    drawnow
    % Capture the plot as an image
    frame = getframe(h);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    
    disp(['Writing ' num2str(in) '/' num2str(length(n)) '...'])
    
    %imwrite(imind,cm,['gif/movie.' num2str(in,'%03d') '.png'])
    
    % Write to the GIF File
    if in == 1
        imwrite(imind,cm,filename,'gif','Loopcount',inf,'DelayTime',dtime);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dtime);
    end
end