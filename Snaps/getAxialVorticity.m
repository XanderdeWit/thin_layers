function ome_z=getAxialVorticity(Vx,Vy,dx,dy)
%Make Vx periodic in y-direction and vice-versa
VxPer=Vx; VxPer(:,end+1,:)=Vx(:,1,:);
VyPer=Vy; VyPer(end+1,:,:)=Vy(1,:,:);

dVx = diff(VxPer,1,2);
dVy = diff(VyPer,1,1);

ome_z = dVy./dx - dVx./dy;

end
