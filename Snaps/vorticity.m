%snap_dir='snaps/Nu1e-3_nxz512_ny16_Qy32/';
%snap_dir='snaps/Redouble/OUT/';
snap_dir='snaps/movie/';

snap='hd3Dwy.stitch.020.mat';

%Qy=32;
%Qy=12; %use different Qy for visual purposes
Qy=16.6;

load([gDataDir snap_dir snap])

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')