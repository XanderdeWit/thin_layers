# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

#alpha=0.33
alpha=0.05
n=np.linspace(1, 150,num=150)

ci_l=2*n/scipy.stats.chi2.ppf(1-alpha/2,2*n)-1
ci_u=2*n/scipy.stats.chi2.ppf(alpha/2,2*n)-1

cin_l=scipy.stats.norm.ppf(alpha/2)/np.sqrt(n)
cin_u=scipy.stats.norm.ppf(1-alpha/2)/np.sqrt(n)


plt.figure()
plt.plot(n, 100*ci_u,'b-',label='$\chi^2$ (exact)')
plt.plot(n, 100*ci_l,'b-')
plt.plot(n, 100*cin_u,'r--',label='normal')
plt.plot(n, 100*cin_l,'r--')
plt.xlabel('$n$')
plt.ylabel(str(int((1-alpha)*100))+'%-CI (%)')
plt.ylim((-50,50))
plt.grid()
plt.legend()
plt.show()