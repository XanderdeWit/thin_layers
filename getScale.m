function scale = getScale(which,eps,l)

if strcmp(which,'time')
    scale=(l^2/eps)^(1/3);
elseif strcmp(which,'energy')
    scale=(eps*l)^(2/3);
else
    disp('Unknown scale')
    scale=0;
end

end

