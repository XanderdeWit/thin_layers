function ts = getTsFromFile(path,ncols,icol_t,icol_q,interp)

file = fopen(path,'r');
data = fscanf(file,'%f %f\n',[ncols Inf]);
fclose('all');

if any(diff(data(icol_t,:))<0)
    disp(['Fixing reset in timeseries ' path])
    resets=find(diff(data(icol_t,:))<0)+1;
    for ireset=1:length(resets)
        reset=resets(ireset);
        reset_time=data(icol_t,reset);
        cut=findIn(data(icol_t,1:reset-1),reset_time);
        data(:,cut:reset-1)=[];
    end
end

ts=timeseries(data(icol_q(1),:)',data(icol_t,:)');
if isa(interp,'logical')
    if interp
        ts=interpTs(ts);
    end
else
    ts=interpTs(ts,interp);
end

if length(icol_q)>1
    for iq=2:length(icol_q)
        ts_iq=timeseries(data(icol_q(iq),:)',data(icol_t,:)');
        if isa(interp,'logical')
            if interp
                ts_iq=interpTs(ts_iq);
            end
        else
            ts_iq=interpTs(ts_iq,interp);
        end
        ts=[ts;ts_iq];
    end
end

end

