# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

#data_vx=np.load('data/hd3Dvx_r9.stitch.005.npy')
#data_vz=np.load('data/hd3Dvz_r9.stitch.005.npy')
data_vx=np.load('data/hd3Dvx.stitch.005.npy')
data_vz=np.load('data/hd3Dvz.stitch.005.npy')

#2D flow
vx_2D=np.mean(data_vx,axis=2)
vz_2D=np.mean(data_vz,axis=2)

w_2D=(np.size(data_vx,0)/(2*np.pi))*(1/2)*((np.roll(vx_2D,-1,axis=0)-np.roll(vx_2D,1,axis=0))-(np.roll(vz_2D,-1,axis=1)-np.roll(vz_2D,1,axis=1)))

lim=0.8*np.amax(np.abs(w_2D))

plt.figure()
plt.pcolor(w_2D, shading='auto',vmin=-lim,vmax=lim)
plt.colorbar()
plt.xlabel('$x$')
plt.ylabel('$y$')
