all_series={'F8R76/PDFs/Q83/','F8R76/PDFs/Q84/','F8R76/PDFs/Q85/'};
Qs=[8.3,8.4,8.5]/8.0;

figure
hold on
%togethers
for iser=1:length(all_series)
    [pdfTogether,~]=getBimodalPDF([gDataDir char(all_series(iser))],'ks');
    plot(pdfTogether(1,:),pdfTogether(2,:),'LineWidth',2,'DisplayName',['$Q=' num2str(Qs(iser),3) '$'])
end
legend
%individuals
% for iser=1:length(all_series)
%     [~,pdfIndividual]=getBimodalPDF([gDataDir char(all_series(iser))],'ks');
%     for irun=1:size(pdfIndividual,1)
%         plot(squeeze(pdfIndividual(irun,1,:)),squeeze(pdfIndividual(irun,2,:)),'LineWidth',2,'Color',0.8+0.2*gPlotColors(iser,:))
%     end
% end
% set(gca,'Children',flip(get(gca,'Children'))) %make sure transparent values are on background
xlabel('$E_{ls}$')
ylabel('PDF $E_{ls}$')
grid on
box on