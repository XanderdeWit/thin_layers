function [all_buildupTimes,all_decayTimes] = getBimodalWaitTimes(seriespath,plotTs)

%find maxima of PDF to define thresholds
[pdfTogether,~]=getBimodalPDF(seriespath,'ks');
maxima = pdfTogether(1,islocalmax(pdfTogether(2,:)));
if length(maxima)>2
    disp('Warning, more than 2 maxima found, selected first and last')
    disp(maxima)
end
nonLSV=maxima(1); LSV=maxima(end);

%get buildup and decay times from all timeseries
dirs=dir(seriespath);
dirs=dirs(3:end);

nsims=length(dirs);

all_buildupTimes=[]; all_decayTimes=[];

for icase=1:nsims
    tsE2D_ls=getTsFromFile([seriespath dirs(icase).name '/energy2D_ls_balance.txt'],7,1,2,false);
    
    [buildupTimes,decayTimes]=analyzeTs(tsE2D_ls,nonLSV,LSV);
    
    all_buildupTimes=[all_buildupTimes,buildupTimes]; all_decayTimes=[all_decayTimes,decayTimes];
    
    if plotTs
        figure
        plot(tsE2D_ls.Time,tsE2D_ls.Data)
        hold on
        tevent=0;
        for ie=1:(length(buildupTimes)+length(decayTimes))
            if mod(ie,2) %if odd, it is buildup, else decay
                tevent=tevent+buildupTimes((ie+1)/2);
                xline(tevent,'--b');
            else
                tevent=tevent+decayTimes(ie/2);
                xline(tevent,'--r');
            end
        end
    end
end

end


function [buildupTimes,decayTimes] = analyzeTs(ts,th_nonLSV,th_LSV)

LSV=(ts.Data(1)>=th_LSV);

tevent=0;

nbuildups=0; ndecays=0;

for it=1:length(ts.Time)
    if LSV
        if ts.Data(it)<=th_nonLSV
            ndecays=ndecays+1;
            decayTimes(ndecays)=ts.Time(it)-tevent;
            tevent=ts.Time(it);
            LSV=false;
        end
    else
        if ts.Data(it)>=th_LSV
            nbuildups=nbuildups+1;
            buildupTimes(nbuildups)=ts.Time(it)-tevent;
            tevent=ts.Time(it);
            LSV=true;
        end
    end
end

end

