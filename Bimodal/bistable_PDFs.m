runL2='K9_PDFs/run142';
runH2='K9_PDFs/run142H';

tsL2=getTsFromFile([gDataDir runL2 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH2=getTsFromFile([gDataDir runH2 '/energy2D_ls_balance.txt'],7,1,2,true);

figure
plot(tsL2.Time,tsL2.Data,'Color',gPlotColors(1,:))
hold on
plot(tsH2.Time,tsH2.Data,'Color',gPlotColors(1,:))
grid on
box on
set(gca, 'XLimSpec', 'Tight');
xlabel('$t$')
ylabel('$E_{ls}$')

runL1='K9_PDFs/run140';
runH1='K9_PDFs/run140H';

tsL1=getTsFromFile([gDataDir runL1 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH1=getTsFromFile([gDataDir runH1 '/energy2D_ls_balance.txt'],7,1,2,true);

runL3='K9_PDFs/run144';
runH3='K9_PDFs/run144H';

tsL3=getTsFromFile([gDataDir runL3 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH3=getTsFromFile([gDataDir runH3 '/energy2D_ls_balance.txt'],7,1,2,true);

tsLs=[tsL1,tsL2,tsL3]; tsHs=[tsH1,tsH2,tsH3];
figure
hold on
icol=[2,1,3];
Qs=[14.0,14.2,14.4]/9;
for its=1:3
    %calculate PDF
    %hist
    % [epdfL,edges] = histcounts(tsLs(its).Data,50,'Normalization', 'pdf');
    % xiL=0.5*(edges(2:end)+edges(1:end-1));
    % [epdfH,edges] = histcounts(tsHs(its).Data,50,'Normalization', 'pdf');
    % xiH=0.5*(edges(2:end)+edges(1:end-1));
    %ksdensity
    [epdfL,xiL]=ksdensity(tsLs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    [epdfH,xiH]=ksdensity(tsHs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    
    %gaussian to tsH
    disp(['Gaussian to tsH Q=' num2str(Qs(its),'%.2f') ' is mu=' num2str(mean(tsHs(its).Data)) ' sig=' num2str(std(tsHs(its).Data))])

    %plot PDFs
    ps(its)=plot(xiL,epdfL,'LineWidth',2,'Color',gPlotColors(icol(its),:),'DisplayName',['$Q=' num2str(Qs(its),'%.2f') '$']);
    plot(xiH,epdfH,'LineWidth',2,'Color',gPlotColors(icol(its),:))
    
    xiHext=linspace(0.95*xiH(1),1.05*xiH(end),100);
    %plot gaussian
    plot(xiHext,normpdf(xiHext,mean(tsHs(its).Data),std(tsHs(its).Data)),'--','LineWidth',2,'Color',gPlotColors(icol(its),:))
end
legend(ps,'Location','southeast')
cs=get(gca,'Children');
set(gca,'Children',cs([4,5,6,1,2,3,7,8,9]));
xlabel('$E_{ls}$')
ylabel('PDF $E_{ls}$')
grid on
box on
set(gca, 'YScale', 'log')


runLK10='K9_PDFs/run1578K10';
runHK10='K9_PDFs/run1578K10H';

tsLK10=getTsFromFile([gDataDir runLK10 '/energy2D_ls_balance.txt'],7,1,2,true);
tsHK10=getTsFromFile([gDataDir runHK10 '/energy2D_ls_balance.txt'],7,1,2,true);

tsLs=[tsL2,tsLK10]; tsHs=[tsH2,tsHK10];
Ks=[9,10];
lines={'-','--'};
figure
hold on
for its=1:2
    %calculate PDF
    %hist
    % [epdfL,edges] = histcounts(tsLs(its).Data,50,'Normalization', 'pdf');
    % xiL=0.5*(edges(2:end)+edges(1:end-1));
    % [epdfH,edges] = histcounts(tsHs(its).Data,50,'Normalization', 'pdf');
    % xiH=0.5*(edges(2:end)+edges(1:end-1));
    %ksdensity
    [epdfL,xiL]=ksdensity(tsLs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    [epdfH,xiH]=ksdensity(tsHs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    
    %gaussian to tsH
    disp(['Gaussian to tsH 1/K=' num2str(Ks(its),'%.2f') ' is mu=' num2str(mean(tsHs(its).Data)) ' sig=' num2str(std(tsHs(its).Data))])
    energyscale=getScale('energy',0.5,2*pi/Ks(its));
    disp(['Rescaled Gaussian to tsH 1/K=' num2str(Ks(its),'%.2f') ' is mu=' num2str(mean(tsHs(its).Data)/energyscale) ' sig=' num2str(std(tsHs(its).Data)/energyscale)])

    %plot PDFs
    p2s(its)=plot(xiL,epdfL,char(lines{its}),'LineWidth',2,'Color',gPlotColors(1,:),'DisplayName',['$1/K=' num2str(Ks(its)) '$']);
    plot(xiH,epdfH,char(lines{its}),'LineWidth',2,'Color',gPlotColors(1,:))
end
legend(p2s,'Location','southeast')
%cs=get(gca,'Children');
%set(gca,'Children',cs([3,4,1,2,5,6]));
xlabel('$E_{ls}$')
ylabel('PDF $E_{ls}$')
grid on
box on
set(gca, 'YScale', 'log')

