series='F8R76/PDFs/Q84/';

[buildupTimes,decayTimes] = getBimodalWaitTimes([gDataDir series],true);

[mu_bup,dmu_bup,t0_bup,mu_lfit_bup,t0_lfit_bup,~,~,~,~]=getDistMuT0(buildupTimes,true,gPlotColors);
[mu_dec,dmu_dec,t0_dec,mu_lfit_dec,t0_lfit_dec,~,~,~,~]=getDistMuT0(decayTimes,true,gPlotColors);


