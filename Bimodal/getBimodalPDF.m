function [pdfTogether,pdfIndividual] = getBimodalPDF(seriespath,type)

dirs=dir(seriespath);
dirs=dirs(3:end);

nsims=length(dirs);

for icase=1:nsims
    tssE2D_ls(icase)=getTsFromFile([seriespath dirs(icase).name '/energy2D_ls_balance.txt'],7,1,2,true);
end

%together
dataTogether=tssE2D_ls(1).Data;
for icase=2:nsims
    dataTogether=[dataTogether; tssE2D_ls(icase).Data];
end
if strcmp(type,'hist')
    [epdf,edges] = histcounts(dataTogether,100,'Normalization', 'pdf');
    xi=0.5*(edges(2:end)+edges(1:end-1));
elseif strcmp(type,'ks')
    [epdf,xi]=ksdensity(dataTogether,'Support','positive','BoundaryCorrection','reflection');
end
pdfTogether=[xi;epdf];

%individual
for icase=1:nsims
    if strcmp(type,'hist')
        [epdf,edges] = histcounts(tssE2D_ls(icase).Data,100,'Normalization', 'pdf');
        xi=0.5*(edges(2:end)+edges(1:end-1));
    elseif strcmp(type,'ks')
        [epdf,xi]=ksdensity(tssE2D_ls(icase).Data,'Support','positive','BoundaryCorrection','reflection');
    end
    pdf=[xi;epdf];
    pdfIndividual(icase,:,:)=pdf;
end

end

