all_series={'F8R76/PDFs/Q83/','F8R76/PDFs/Q84/','F8R76/PDFs/Q85/'};
Qs=[8.3,8.4,8.5]/8.0;

mus_bup=zeros(1,length(all_series)); mus_dec=zeros(1,length(all_series));
dmus_bup=zeros(2,length(all_series)); dmus_dec=zeros(2,length(all_series));
t0s_bup=zeros(1,length(all_series)); t0s_dec=zeros(1,length(all_series));
mu_lfits_bup=zeros(1,length(all_series)); mu_lfits_dec=zeros(1,length(all_series));
t0_lfits_bup=zeros(1,length(all_series)); t0_lfits_dec=zeros(1,length(all_series));
for iser=1:length(all_series)
    [buildupTimes,decayTimes]=getBimodalWaitTimes([gDataDir char(all_series(iser))],false);
    
    [mus_bup(iser),dmus_bup(:,iser),t0s_bup(iser),mu_lfits_bup(iser),t0_lfits_bup(iser),~,~,~,~]=getDistMuT0(buildupTimes,false);
    [mus_dec(iser),dmus_dec(:,iser),t0s_dec(iser),mu_lfits_dec(iser),t0_lfits_dec(iser),~,~,~,~]=getDistMuT0(decayTimes,false);
    
end

figure
errorbar(Qs,mus_bup,dmus_bup(1,:).*mus_bup,dmus_bup(2,:).*mus_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
hold on
errorbar(Qs,mus_dec,dmus_dec(1,:).*mus_dec,dmus_dec(2,:).*mus_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
errorbar(Qs,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
errorbar(Qs,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\mu(\tau)$')

figure
errorbar(Qs,mus_bup+t0s_bup,dmus_bup(1,:).*(mus_bup+t0s_bup),dmus_bup(2,:).*(mus_bup+t0s_bup),'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
hold on
errorbar(Qs,mus_dec+t0s_dec,dmus_dec(1,:).*(mus_dec+t0s_dec),dmus_dec(2,:).*(mus_dec+t0s_dec),'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
legend
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\textrm{mean}(\tau)$')

figure
scatter(Qs,t0s_bup,80,gPlotColors(2,:),'filled','o','DisplayName','build-up')
hold on
scatter(Qs,t0s_dec,80,gPlotColors(1,:),'filled','d','DisplayName','decay')
scatter(Qs,t0_lfits_bup,80,0.75+0.25*gPlotColors(2,:),'filled','o','DisplayName','Lfit build-up')
scatter(Qs,t0_lfits_dec,80,0.75+0.25*gPlotColors(1,:),'filled','d','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
xlabel('$Q$')
ylabel('$\tau_0$')

