series='F8R76/PDFs/Q84/';

[pdfTogether,pdfIndividual]=getBimodalPDF([gDataDir series],'ks');

figure
hold on
for irun=1:size(pdfIndividual,1)
    plot(squeeze(pdfIndividual(irun,1,:)),squeeze(pdfIndividual(irun,2,:)),'LineWidth',2,'Color',0.8+0.2*gPlotColors(1,:))
end
plot(pdfTogether(1,:),pdfTogether(2,:),'LineWidth',2,'Color',[0 0.4470 0.7410])
xlabel('$E_{ls}$')
ylabel('PDF $E_{ls}$')
grid on
box on
title(series)
%xline(q_low,'r--','LineWidth',1)
%xline(q_high,'r--','LineWidth',1)



% [epdf,edges] = histcounts(tsE2D_ls.Data,100,'Normalization', 'pdf');
% xi=0.5*(edges(2:end)+edges(1:end-1));
% figure
% plot(xi,epdf,'LineWidth',2);
% %set(gca, 'YScale', 'log')
% grid on
% hold on
% %mu=mean(qs)
% %sigma=std(qs)
% %pd = makedist('Normal','mu',mu,'sigma',sigma);
% %plot(xi,pdf(pd,xi),'--','LineWidth',2)
% %ylim([10^floor(log10(min(epdf))),10^ceil(log10(max(epdf)))])
% xlabel('$E_{ls}$')
% ylabel('PDF $E_{ls}$')
% title(simname)
% %xline(q_low,'r--','LineWidth',1)
% %xline(q_high,'r--','LineWidth',1)

% figure
% [f,xi]=ksdensity(tsE2D_ls.Data);
% plot(xi,f,'LineWidth',2)
% xlabel('$E_{ls}$')
% ylabel('PDF $E_{ls}$')
% grid on
% title(simname)
% %xline(q_low,'r--','LineWidth',1)
% %xline(q_high,'r--','LineWidth',1)

