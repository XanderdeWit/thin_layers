# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

data=np.load('data/thin2D_ww.stitch.006.npy')

lim=0.8*np.amax(np.abs(data))

plt.figure()
plt.pcolor(data, shading='auto',cmap='RdBu',vmin=-lim,vmax=lim)
plt.colorbar()
plt.xlabel('$x$')
plt.ylabel('$y$')
