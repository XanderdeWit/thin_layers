files=dir([gDataDir 'adrian/']);

dc_times={};
Qs_decay=[];
ns_dc=[];
i=1;
for ifile=1:length(files)
    if files(ifile).name(1)=='Q' && endsWith(files(ifile).name,'decaytimes.txt')
        file = fopen([gDataDir 'adrian/' files(ifile).name],'r');
        dc_times{i} = fscanf(file,'%f %f\n',[1 Inf]);
        Qs_decay(i)=str2double(files(ifile).name(2:7));
        ns_dc(i)=length(dc_times{i});
        i=i+1;
    end
end

disp(Qs_decay)
disp(ns_dc)

file = fopen([gDataDir '/adrian/adrian_decay.txt'],'r');
data_dc = fscanf(file,'%f %f\n',[3 Inf]);
data_dc = data_dc(:,2:end);

Qs_decay=data_dc(1,:);

Qs_ad_dc=6; %take the last 6 Qs from adrians results, because not enought statistics
%Qs_ad_dc=8; %take the last 8 Qs from adrians results, because not enought statistics

mus_dec(Qs_ad_dc:length(Qs_decay))=data_dc(2,Qs_ad_dc:length(Qs_decay)); mu_lfits_dec(Qs_ad_dc:length(Qs_decay))=data_dc(2,Qs_ad_dc:length(Qs_decay));
dmus_dec(1,Qs_ad_dc:length(Qs_decay))=data_dc(3,Qs_ad_dc:length(Qs_decay))/data_dc(2,Qs_ad_dc:length(Qs_decay)); dmus_dec(2,Qs_ad_dc:length(Qs_decay))=data_dc(3,Qs_ad_dc:length(Qs_decay))/data_dc(2,Qs_ad_dc:length(Qs_decay));
t0s_dec(Qs_ad_dc:length(Qs_decay))=0; t0_lfits_dec(Qs_ad_dc:length(Qs_decay))=0;

for iQ=1:(length(Qs_decay)-Qs_ad_dc)
    [mus_dec(iQ),dmus_dec(:,iQ),t0s_dec(iQ),mu_lfits_dec(iQ),t0_lfits_dec(iQ),~,~,~,~]=getDistMuT0(unique(dc_times{iQ}),false,gPlotColors);
    %title(['Q=' num2str(Qs_decay(iQ))])
end

Qs_decay=Qs_decay/8.0;

[Q0_dc,c_dc,powr_dc,dQ0_dc]=getWTfit(Qs_decay,mus_dec,dmus_dec,false);
[Q0_lfit_dc,c_lfit_dc,~,dQ0_lfit_dc]=getWTfit(Qs_decay,mu_lfits_dec,dmus_dec,false);

if false
    figure
    hold on
    errorbar(Qs_decay,mus_dec,dmus_dec(1,:).*mus_dec,dmus_dec(2,:).*mus_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
    errorbar(Qs_decay,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    set(gca, 'YScale', 'log')
    xlabel('$Q$')
    ylabel('$\mu(\tau)$')

    figure
    hold on
    errorbar(Qs_decay,(1./mus_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mus_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mus_dec).^powr_dc,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
    errorbar(Qs_decay,(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mu_lfits_dec).^powr_dc,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    plot(Qs_decay,c_lfit_dc*Qs_decay-c_lfit_dc*Q0_lfit_dc,'--','Color',0.75+0.25*gPlotColors(1,:),'LineWidth',2)
    plot(Qs_decay,c_dc*Qs_decay-c_dc*Q0_dc,'--','Color',gPlotColors(1,:),'LineWidth',2)
    errorbar(Q0_lfit_dc,0,dQ0_lfit_dc,'horizontal','x','Color',0.75+0.25*gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
    errorbar(Q0_dc,0,dQ0_dc,'horizontal','x','Color',gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
    grid on
    box on
    xlabel('$Q$')
    ylabel('$1/\mu^{1/2}(\tau)$')
    ylim([0 inf])

    figure
    hold on
    errorbar(Qs_decay,mus_dec+t0s_dec,dmus_dec(1,:).*(mus_dec+t0s_dec),dmus_dec(2,:).*(mus_dec+t0s_dec),'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
    errorbar(Qs_decay,data_dc(2,:),data_dc(3,:),data_dc(3,:),'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay Adrian')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    set(gca, 'YScale', 'log')
    xlabel('$Q$')
    ylabel('$\textrm{mean}(\tau)$')

    figure
    hold on
    scatter(Qs_decay,t0s_dec,80,gPlotColors(1,:),'filled','d','DisplayName','decay')
    scatter(Qs_decay,t0_lfits_dec,80,0.75+0.25*gPlotColors(1,:),'filled','d','DisplayName','Lfit decay')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    xlabel('$Q$')
    ylabel('$\tau_0$')
end