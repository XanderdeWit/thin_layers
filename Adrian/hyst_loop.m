file = fopen([gDataDir 'adrian/data_lower.txt'],'r');
data_lower = fscanf(file,'%f %f\n',[2 Inf]);
file = fopen([gDataDir 'adrian/data_upper.txt'],'r');
data_upper = fscanf(file,'%f %f\n',[2 Inf]);

figure
plot(data_lower(1,:)/8.0,data_lower(2,:),'d--','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:))
hold on
plot(data_upper(1,:)/8.0,data_upper(2,:),'s--','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:))
grid on
box on
xlabel('$Q$')
ylabel('$E_{ls}$')
xlim([1.5,1.68])