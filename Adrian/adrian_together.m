adrian_buildups;
adrian_decays;

figure
hold on
errorbar(Qs_buildup,mus_bup,dmus_bup(1,:).*mus_bup,dmus_bup(2,:).*mus_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
errorbar(Qs_decay,mus_dec,dmus_dec(1,:).*mus_dec,dmus_dec(2,:).*mus_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
errorbar(Qs_buildup,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
errorbar(Qs_decay,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\mu(\tau)$')

figure
hold on
yyaxis right
errorbar(Qs_buildup,(1./mus_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mus_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mus_bup).^powr_bu,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
yyaxis left
errorbar(Qs_decay,(1./mus_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mus_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mus_dec).^powr_dc,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
yyaxis right
errorbar(Qs_buildup,(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mu_lfits_bup).^powr_bu,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
yyaxis left
errorbar(Qs_decay,(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mu_lfits_dec).^powr_dc,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
legend
yyaxis right; set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
yyaxis left; set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
yyaxis right
plot(Qs_buildup,c_lfit_bu*Qs_buildup-c_lfit_bu*Q0_lfit_bu,'--','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2)
plot(Qs_buildup,c_bu*Qs_buildup-c_bu*Q0_bu,'--','Color',gPlotColors(2,:),'LineWidth',2)
errorbar(Q0_lfit_bu,0,dQ0_lfit_bu,'horizontal','x','Color',0.75+0.25*gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
errorbar(Q0_bu,0,dQ0_bu,'horizontal','x','Color',gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
yyaxis left
plot(Qs_decay,c_lfit_dc*Qs_decay-c_lfit_dc*Q0_lfit_dc,'--','Color',0.75+0.25*gPlotColors(1,:),'LineWidth',2)
plot(Qs_decay,c_dc*Qs_decay-c_dc*Q0_dc,'--','Color',gPlotColors(1,:),'LineWidth',2)
errorbar(Q0_lfit_dc,0,dQ0_lfit_dc,'horizontal','x','Color',0.75+0.25*gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
errorbar(Q0_dc,0,dQ0_dc,'horizontal','x','Color',gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
xlabel('$Q$')
yyaxis left
ylim([0 inf])
ylabel('$1/\mu^{1/2}(\tau)$')
yyaxis right
ylim([0 inf])
ylabel('$1/\mu^{1/3}(\tau)$')
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

figure
hold on
errorbar(Qs_buildup,mus_bup+t0s_bup,dmus_bup(1,:).*(mus_bup+t0s_bup),dmus_bup(2,:).*(mus_bup+t0s_bup),'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
errorbar(Qs_decay,mus_dec+t0s_dec,dmus_dec(1,:).*(mus_dec+t0s_dec),dmus_dec(2,:).*(mus_dec+t0s_dec),'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
errorbar(Qs_buildup,data_bu(2,1:end-3),data_bu(3,1:end-3),data_bu(3,1:end-3),'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','buildup Adrian')
errorbar(Qs_decay,data_dc(2,:),data_dc(3,:),data_dc(3,:),'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay Adrian')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\textrm{mean}(\tau)$')

figure
hold on
scatter(Qs_buildup,t0s_bup,80,gPlotColors(2,:),'filled','o','DisplayName','build-up')
scatter(Qs_decay,t0s_dec,80,gPlotColors(1,:),'filled','d','DisplayName','decay')
scatter(Qs_buildup,t0_lfits_bup,80,0.75+0.25*gPlotColors(2,:),'filled','o','DisplayName','Lfit build-up')
scatter(Qs_decay,t0_lfits_dec,80,0.75+0.25*gPlotColors(1,:),'filled','d','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
xlabel('$Q$')
ylabel('$\tau_0$')