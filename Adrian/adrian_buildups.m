files=dir([gDataDir 'adrian/']);

bu_times={};
Qs_buildup=[];
ns_bu=[];
i=1;
for ifile=1:length(files)
    if files(ifile).name(1)=='Q' && endsWith(files(ifile).name,'builduptimes.txt')
        file = fopen([gDataDir 'adrian/' files(ifile).name],'r');
        bu_times{i} = fscanf(file,'%f %f\n',[1 Inf]);
        Qs_buildup(i)=str2double(files(ifile).name(2:7));
        ns_bu(i)=length(bu_times{i});
        i=i+1;
    end
end

disp(Qs_buildup)
disp(ns_bu)

file = fopen([gDataDir '/adrian/adrian_buildup.txt'],'r');
data_bu = fscanf(file,'%f %f\n',[3 Inf]);

Qs_ad_bu=6; %take the first 6 Qs from adrians results, because not enought statistics

mus_bup(1:Qs_ad_bu)=data_bu(2,1:Qs_ad_bu); mu_lfits_bup(1:Qs_ad_bu)=data_bu(2,1:Qs_ad_bu);
dmus_bup(1,1:Qs_ad_bu)=data_bu(3,1:Qs_ad_bu)/data_bu(2,1:Qs_ad_bu); dmus_bup(2,1:Qs_ad_bu)=data_bu(3,1:Qs_ad_bu)/data_bu(2,1:Qs_ad_bu);
t0s_bup(1:Qs_ad_bu)=0; t0_lfits_bup(1:Qs_ad_bu)=0;

for iQ=(Qs_ad_bu+1):length(Qs_buildup)
    [mus_bup(iQ),dmus_bup(:,iQ),t0s_bup(iQ),mu_lfits_bup(iQ),t0_lfits_bup(iQ),~,~,~,~]=getDistMuT0(unique(bu_times{iQ}),false,gPlotColors);
    %title(['Q=' num2str(Qs_buildup(iQ))])
end

Qs_buildup=Qs_buildup/8.0;

[Q0_bu,c_bu,powr_bu,dQ0_bu]=getWTfit(Qs_buildup,mus_bup,dmus_bup,true);
[Q0_lfit_bu,c_lfit_bu,~,dQ0_lfit_bu]=getWTfit(Qs_buildup,mu_lfits_bup,dmus_bup,true);

if false
    figure
    hold on
    errorbar(Qs_buildup,mus_bup,dmus_bup(1,:).*mus_bup,dmus_bup(2,:).*mus_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
    errorbar(Qs_buildup,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    set(gca, 'YScale', 'log')
    xlabel('$Q$')
    ylabel('$\mu(\tau)$')

    figure
    hold on
    errorbar(Qs_buildup,(1./mus_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mus_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mus_bup).^powr_bu,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
    errorbar(Qs_buildup,(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mu_lfits_bup).^powr_bu,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    plot(Qs_buildup,c_lfit_bu*Qs_buildup-c_lfit_bu*Q0_lfit_bu,'--','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2)
    plot(Qs_buildup,c_bu*Qs_buildup-c_bu*Q0_bu,'--','Color',gPlotColors(2,:),'LineWidth',2)
    errorbar(Q0_lfit_bu,0,dQ0_lfit_bu,'horizontal','x','Color',0.75+0.25*gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
    errorbar(Q0_bu,0,dQ0_bu,'horizontal','x','Color',gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
    grid on
    box on
    xlabel('$Q$')
    ylabel('$1/\mu^{1/3}(\tau)$')

    figure
    hold on
    errorbar(Qs_buildup,mus_bup+t0s_bup,dmus_bup(1,:).*(mus_bup+t0s_bup),dmus_bup(2,:).*(mus_bup+t0s_bup),'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
    errorbar(Qs_buildup,data_bu(2,1:end-3),data_bu(3,1:end-3),data_bu(3,1:end-3),'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','buildup Adrian')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    set(gca, 'YScale', 'log')
    xlabel('$Q$')
    ylabel('$\textrm{mean}(\tau)$')

    figure
    hold on
    scatter(Qs_buildup,t0s_bup,80,gPlotColors(2,:),'filled','o','DisplayName','build-up')
    scatter(Qs_buildup,t0_lfits_bup,80,0.75+0.25*gPlotColors(2,:),'filled','o','DisplayName','Lfit build-up')
    legend
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    grid on
    box on
    xlabel('$Q$')
    ylabel('$\tau_0$')
end