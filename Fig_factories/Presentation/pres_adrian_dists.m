set(0,'DefaultAxesFontSize',20)

adrian_decays;
adrian_buildups;

timescale=getScale('time',0.5,2*pi/8.0);

[~,~,~,mu_lfit,t0_lfit,m_ecdf_x,m_ecdf,m_ecdf_lo,m_ecdf_up]=getDistMuT0(dc_times{7}/timescale,false);

figure
stairs(m_ecdf_x,m_ecdf,'-','Color',gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.50$')
legend('Location','southeast')
hold on
stairs(m_ecdf_x,[m_ecdf_lo, m_ecdf_up],':','Color',gPlotColors(1,:),'LineWidth',1.5)
%plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0)/mu),'--','Color',gPlotColors(2,:),'LineWidth',2.5)
plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',gPlotColors(2,:),'LineWidth',2.5)
%plot(dfit_x,dfit_y,'r.','MarkerSize',10)
xlabel('Decay time $t_\mathrm{d}$')
ylabel('Empirical CDF($t_\mathrm{d}$)')
ylim([0,1])
xlim([0 inf])
grid on
box on

saveas(gcf,'Fig_factories/Presentation/figs/adrian_dists_ex','epsc')

% figure
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{10}/timescale,false);
% stairs(m_ecdf_x,m_ecdf,'-','Color',0.8*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.72$')
% hold on
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{11}/timescale,false);
% stairs(m_ecdf_x,m_ecdf,'-','Color',0.2+0.8*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.75$')
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{4}/timescale,false);
% stairs(m_ecdf_x,m_ecdf,'-','Color',0.2+0.8*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.47$')
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{6}/timescale,false);
% stairs(m_ecdf_x,m_ecdf,'-','Color',0.8*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.49$')
% legend('Location','southeast')
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{10}/timescale,false);
% plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.8*gPlotColors(2,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{11}/timescale,false);
% plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.2+0.8*gPlotColors(2,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{4}/timescale,false);
% plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.2+0.8*gPlotColors(1,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{6}/timescale,false);
% plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.8*gPlotColors(1,:),'LineWidth',2)
% xlabel('$t_{\mathrm{b},\mathrm{d}}/\tau_f$')
% ylabel('Empirical CDF($t_{\mathrm{b},\mathrm{d}}$)')
% ylim([0,1])
% xlim([0 inf])
% grid on
% box on

% figure
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{8}/timescale,false);
% stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.7*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.66$')
% hold on
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{11}/timescale,false);
% stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.3+0.7*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.75$')
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{3}/timescale,false);
% stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.3+0.7*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.46$')
% [~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{7}/timescale,false);
% stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.7*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.50$')
% legend('Location','southwest')
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{8}/timescale,false);
% plot(m_ecdf_x,exp(-(m_ecdf_x-(2/3)*t0_lfit)/(1.2*mu_lfit)),'--','Color',0.7*gPlotColors(2,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{11}/timescale,false);
% plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.3+0.7*gPlotColors(2,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{3}/timescale,false);
% plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.3+0.7*gPlotColors(1,:),'LineWidth',2)
% [~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{7}/timescale,false);
% plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.7*gPlotColors(1,:),'LineWidth',2)
% set(gca, 'YScale', 'log')
% xlabel('$t_{\mathrm{b},\mathrm{d}}/\tau_f$')
% ylabel('Empirical $1-$CDF($t_{\mathrm{b},\mathrm{d}}$)')
% ylim([0,1])
% %xlim([0 inf])
% xlim([0 3000])
% grid on
% box on
% 
% saveas(gcf,'Fig_factories/Presentation/figs/adrian_dists','epsc')
