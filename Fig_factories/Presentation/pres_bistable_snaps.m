%% 3D state

snap_dir='movies/testrun_movie5/';

snap='hd3Dwy.stitch.031.mat';

Qy=16;

load([gDataDir snap_dir snap])

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

valueLim=1.0*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
xlabel('')
ylabel('')
zlabel('')
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')
colorbar off

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.9*p(4)),p(4)])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Presentation/figs/bistable_snap_1','-dpng','-opengl','-r600')

%% Condensate

snap_dir='movies/testrun_movie5/';

snap='hd3Dwy.stitch.730.mat';

Qy=16;

load([gDataDir snap_dir snap])

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

%valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
xlabel('')
ylabel('')
zlabel('')
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')
colorbar off

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.9*p(4)),p(4)])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Presentation/figs/bistable_snap_2','-dpng','-opengl','-r600')