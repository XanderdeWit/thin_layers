set(0,'DefaultAxesFontSize',20)

%% BIMODAL

all_series={'F8R76/PDFs/Q83/','F8R76/PDFs/Q84/','F8R76/PDFs/Q85/'};
Qs=[8.3,8.4,8.5]/8.0;

energyscale=getScale('energy',0.5,2*pi/8.0);
timescale=getScale('time',0.5,2*pi/8.0);

figure
ts=getTsFromFile([gDataDir 'F8R76/PDFs/Q84/' 'run1' '/energy2D_ls_balance.txt'],7,1,2,true);
plot(ts.Time/timescale-1e4,ts.Data/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
grid on
box on
xlabel('Time $t$')
ylabel('Large-scale energy $E_\mathrm{ls}$')
xlim([0e4,2e4])

saveas(gcf,'Fig_factories/Presentation/figs/bimodal_timeseries','epsc')

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(0.6*p(3)),round(0.6*p(4))])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Presentation/figs/bimodal_timeseries_small','-dpng','-opengl','-r600')

figure
hold on
%togethers
icol=[2,1,3];
for iser=1:length(all_series)
    [pdfTogether,~]=getBimodalPDF([gDataDir char(all_series(iser))],'ks');
    plot(pdfTogether(1,:)/energyscale,pdfTogether(2,:)*energyscale,'LineWidth',2,'DisplayName',['$Q=' num2str(Qs(iser),3) '$'],'Color',gPlotColors(icol(iser),:))
end
legend
xlabel('Large-scale energy $E_\mathrm{ls}$')
ylabel('PDF($E_\mathrm{ls}$)')
grid on
box on

saveas(gcf,'Fig_factories/Presentation/figs/bimodal_pdfs','epsc')

%% PURE HYSTERESIS

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

runL2='K9_PDFs/run142';
runH2='K9_PDFs/run142H';

tsL2=getTsFromFile([gDataDir runL2 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH2=getTsFromFile([gDataDir runH2 '/energy2D_ls_balance.txt'],7,1,2,true);

figure
plot(tsL2.Time/timescale,tsL2.Data/energyscale,'Color',gPlotColors(1,:))
hold on
plot(tsH2.Time/timescale,tsH2.Data/energyscale,'Color',gPlotColors(1,:))
grid on
box on
xlabel('Time $t$')
ylabel('Large-scale energy $E_\mathrm{ls}$')
xlim([0e4,2e4])

saveas(gcf,'Fig_factories/Presentation/figs/purehyst_timeseries','epsc')

runL1='K9_PDFs/run140';
runH1='K9_PDFs/run140H';

tsL1=getTsFromFile([gDataDir runL1 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH1=getTsFromFile([gDataDir runH1 '/energy2D_ls_balance.txt'],7,1,2,true);

runL3='K9_PDFs/run144';
runH3='K9_PDFs/run144H';

tsL3=getTsFromFile([gDataDir runL3 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH3=getTsFromFile([gDataDir runH3 '/energy2D_ls_balance.txt'],7,1,2,true);

tsLs=[tsL1,tsL2,tsL3]; tsHs=[tsH1,tsH2,tsH3];

figure
hold on
icol=[2,1,3];
Qs=[14.0,14.2,14.4]/9;
for its=1:3
    %calculate PDF
    %hist
    % [epdfL,edges] = histcounts(tsLs(its).Data,50,'Normalization', 'pdf');
    % xiL=0.5*(edges(2:end)+edges(1:end-1));
    % [epdfH,edges] = histcounts(tsHs(its).Data,50,'Normalization', 'pdf');
    % xiH=0.5*(edges(2:end)+edges(1:end-1));
    %ksdensity
    [epdfL,xiL]=ksdensity(tsLs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    [epdfH,xiH]=ksdensity(tsHs(its).Data,'Support','positive','BoundaryCorrection','reflection');

    %plot PDFs
    ps(its)=plot(xiL/energyscale,epdfL*energyscale,'LineWidth',2,'Color',gPlotColors(icol(its),:),'DisplayName',['$Q=' num2str(Qs(its),'%.2f') '$']);
    plot(xiH/energyscale,epdfH*energyscale,'LineWidth',2,'Color',gPlotColors(icol(its),:))
end
legend(ps,'Location','southeast')
cs=get(gca,'Children');
set(gca,'Children',cs([3,4,1,2,5,6]));
xlabel('Large-scale energy $E_{ls}$')
ylabel('PDF($E_{ls}$)')
grid on
box on
set(gca, 'YScale', 'log')
ylim([10^-6,2*10^0])

saveas(gcf,'Fig_factories/Presentation/figs/purehyst_pdfs','epsc')
