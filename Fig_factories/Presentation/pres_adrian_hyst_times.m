set(0,'DefaultAxesFontSize',20)

file = fopen([gDataDir 'adrian/data_lower.txt'],'r');
data_lower = fscanf(file,'%f %f\n',[2 Inf]);
file = fopen([gDataDir 'adrian/data_upper.txt'],'r');
data_upper = fscanf(file,'%f %f\n',[2 Inf]);

energyscale=getScale('energy',0.5,2*pi/8.0);

figure
plot(data_upper(1,9:end)/8.0,0.5*data_upper(2,9:end)/energyscale,'s-','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'DisplayName','Upper branch')
hold on
plot(data_lower(1,1:end-5)/8.0,0.5*data_lower(2,1:end-5)/energyscale,'d-','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'DisplayName','Lower branch')
legend('Location','northwest')
set(gca,'Children',fftshift(get(gca,'Children')))
grid on
box on
xlabel('Thinness $Q$')
ylabel('Large-scale energy $E_\mathrm{ls}$')
xlim([1.4,1.9])

saveas(gcf,'Fig_factories/Presentation/figs/adrian_hyst','epsc')


adrian_buildups;
adrian_decays;

timescale=getScale('time',0.5,2*pi/8.0);

mu_lfits_bup=mu_lfits_bup/timescale; mu_lfits_dec=mu_lfits_dec/timescale;

figure
hold on
errorbar(Qs_buildup,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up')
errorbar(Qs_decay,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Decay')
legend
grid on
box on
set(gca, 'YScale', 'log')
xlabel('Thinness $Q$')
ylabel('Mean waiting time $\tau_W$')

saveas(gcf,'Fig_factories/Presentation/figs/adrian_hyst_tauw','epsc')