%first point
Re_1=76;
all_series_buildup_1=all_series_buildup_F8R76;
Qs_buildup_1=Qs_buildup_F8R76;
all_series_decay_1=all_series_decay_F8R76;
Qs_decay_1=Qs_decay_F8R76;

mus_bup_1=zeros(1,length(all_series_buildup_1)); mus_dec_1=zeros(1,length(all_series_decay_1));
dmus_bup_1=zeros(2,length(all_series_buildup_1)); dmus_dec_1=zeros(2,length(all_series_decay_1));
mu_lfits_bup_1=zeros(1,length(all_series_buildup_1)); mu_lfits_dec_1=zeros(1,length(all_series_decay_1));
for iser=1:length(all_series_buildup_1)
    buildupTimes=getWaitingtimes([gDataDir char(all_series_buildup_1(iser))],true);
    [mus_bup_1(iser),dmus_bup_1(:,iser),~,mu_lfits_bup_1(iser),~,~,~,~,~]=getDistMuT0(buildupTimes,false);
end
for iser=1:length(all_series_decay_1)
    decayTimes=getWaitingtimes([gDataDir char(all_series_decay_1(iser))],false);
    [mus_dec_1(iser),dmus_dec_1(:,iser),~,mu_lfits_dec_1(iser),~,~,~,~,~]=getDistMuT0(decayTimes,false);
end

[Q0_bu_1,c_bu_1,~,dQ0_bu_1]=getWTfit(Qs_buildup_1,mus_bup_1,dmus_bup_1,true);
[Q0_lfit_bu_1,c_lfit_bu_1,~,dQ0_lfit_bu_1]=getWTfit(Qs_buildup_1,mu_lfits_bup_1,dmus_bup_1,true);
[Q0_dc_1,c_dc_1,~,dQ0_dc_1]=getWTfit(Qs_decay_1,mus_dec_1,dmus_dec_1,false);
[Q0_lfit_dc_1,c_lfit_dc_1,~,dQ0_lfit_dc_1]=getWTfit(Qs_decay_1,mu_lfits_dec_1,dmus_dec_1,false);

%second point
Re_2=131;
all_series_buildup_2=all_series_buildup_F8R130;
Qs_buildup_2=Qs_buildup_F8R130;
all_series_decay_2=all_series_decay_F8R130;
Qs_decay_2=Qs_decay_F8R130;

mus_bup_2=zeros(1,length(all_series_buildup_2)); mus_dec_2=zeros(1,length(all_series_decay_2));
dmus_bup_2=zeros(2,length(all_series_buildup_2)); dmus_dec_2=zeros(2,length(all_series_decay_2));
mu_lfits_bup_2=zeros(1,length(all_series_buildup_2)); mu_lfits_dec_2=zeros(1,length(all_series_decay_2));
for iser=1:length(all_series_buildup_2)
    buildupTimes=getWaitingtimes([gDataDir char(all_series_buildup_2(iser))],true);
    [mus_bup_2(iser),dmus_bup_2(:,iser),~,mu_lfits_bup_2(iser),~,~,~,~,~]=getDistMuT0(buildupTimes,false);
end
for iser=1:length(all_series_decay_2)
    decayTimes=getWaitingtimes([gDataDir char(all_series_decay_2(iser))],false);
    [mus_dec_2(iser),dmus_dec_2(:,iser),~,mu_lfits_dec_2(iser),~,~,~,~,~]=getDistMuT0(decayTimes,false);
end

[Q0_bu_2,c_bu_2,~,dQ0_bu_2]=getWTfit(Qs_buildup_2,mus_bup_2,dmus_bup_2,true);
[Q0_lfit_bu_2,c_lfit_bu_2,~,dQ0_lfit_bu_2]=getWTfit(Qs_buildup_2,mu_lfits_bup_2,dmus_bup_2,true);
[Q0_dc_2,c_dc_2,~,dQ0_dc_2]=getWTfit(Qs_decay_2,mus_dec_2,dmus_dec_2,false);
[Q0_lfit_dc_2,c_lfit_dc_2,~,dQ0_lfit_dc_2]=getWTfit(Qs_decay_2,mu_lfits_dec_2,dmus_dec_2,false);

%third point (Adrian data)
Re_3=192;
adrian_buildups;
adrian_decays;
Qs_buildup_3=Qs_buildup; Qs_decay_3=Qs_decay;
mus_bup_3=mus_bup; mus_dec_3=mus_dec;
dmus_bup_3=dmus_bup; dmus_dec_3=dmus_dec;
mu_lfits_bup_3=mu_lfits_bup; mu_lfits_dec_3=mu_lfits_dec;
Q0_bu_3=Q0_bu; Q0_lfit_bu_3=Q0_lfit_bu;
dQ0_bu_3=dQ0_bu; dQ0_lfit_bu_3=dQ0_lfit_bu;
Q0_dc_3=Q0_dc; Q0_lfit_dc_3=Q0_lfit_dc;
dQ0_dc_3=dQ0_dc; dQ0_lfit_dc_3=dQ0_lfit_dc;
c_bu_3=c_bu; c_lfit_bu_3=c_lfit_bu;
c_dc_3=c_dc; c_lfit_dc_3=c_lfit_dc;

%forth point
Re_4=329;
all_series_buildup_4=all_series_buildup_F8R329;
Qs_buildup_4=Qs_buildup_F8R329;
all_series_decay_4=all_series_decay_F8R329;
Qs_decay_4=Qs_decay_F8R329;

mus_bup_4=zeros(1,length(all_series_buildup_4)); mus_dec_4=zeros(1,length(all_series_decay_4));
dmus_bup_4=zeros(2,length(all_series_buildup_4)); dmus_dec_4=zeros(2,length(all_series_decay_4));
mu_lfits_bup_4=zeros(1,length(all_series_buildup_4)); mu_lfits_dec_4=zeros(1,length(all_series_decay_4));
for iser=1:length(all_series_buildup_4)
    buildupTimes=getWaitingtimes([gDataDir char(all_series_buildup_4(iser))],true);
    [mus_bup_4(iser),dmus_bup_4(:,iser),~,mu_lfits_bup_4(iser),~,~,~,~,~]=getDistMuT0(buildupTimes,false);
end
for iser=1:length(all_series_decay_4)
    decayTimes=getWaitingtimes([gDataDir char(all_series_decay_4(iser))],false);
    [mus_dec_4(iser),dmus_dec_4(:,iser),~,mu_lfits_dec_4(iser),~,~,~,~,~]=getDistMuT0(decayTimes,false);
end

[Q0_bu_4,c_bu_4,~,dQ0_bu_4]=getWTfit(Qs_buildup_4,mus_bup_4,dmus_bup_4,true);
[Q0_lfit_bu_4,c_lfit_bu_4,~,dQ0_lfit_bu_4]=getWTfit(Qs_buildup_4,mu_lfits_bup_4,dmus_bup_4,true);
[Q0_dc_4,c_dc_4,~,dQ0_dc_4]=getWTfit(Qs_decay_4,mus_dec_4,dmus_dec_4,false);
[Q0_lfit_dc_4,c_lfit_dc_4,~,dQ0_lfit_dc_4]=getWTfit(Qs_decay_4,mu_lfits_dec_4,dmus_dec_4,false);

%rescale
timescale_1=getScale('time',0.5,2*pi/8.0);
mus_bup_1=mus_bup_1/timescale_1; mus_dec_1=mus_dec_1/timescale_1;
mu_lfits_bup_1=mu_lfits_bup_1/timescale_1; mu_lfits_dec_1=mu_lfits_dec_1/timescale_1;
timescale_2=getScale('time',0.5,2*pi/8.0);
mus_bup_2=mus_bup_2/timescale_2; mus_dec_2=mus_dec_2/timescale_2;
mu_lfits_bup_2=mu_lfits_bup_2/timescale_2; mu_lfits_dec_2=mu_lfits_dec_2/timescale_2;
timescale_3=getScale('time',0.5,2*pi/8.0);
mus_bup_3=mus_bup_3/timescale_3; mus_dec_3=mus_dec_3/timescale_3;
mu_lfits_bup_3=mu_lfits_bup_3/timescale_3; mu_lfits_dec_3=mu_lfits_dec_3/timescale_3;
timescale_4=getScale('time',0.5,2*pi/8.0);
mus_bup_4=mus_bup_4/timescale_4; mus_dec_4=mus_dec_4/timescale_4;
mu_lfits_bup_4=mu_lfits_bup_4/timescale_4; mu_lfits_dec_4=mu_lfits_dec_4/timescale_4;

%colors
color_bu_1=gPlotColors(3,:); color_bu_3=gPlotColors(2,:); color_bu_4=gPlotColors(7,:);
%color_dc_1=0.3+0.7*gPlotColors(6,:); color_dc_3=gPlotColors(1,:); color_dc_4=0.6*gPlotColors(1,:); sym_dc='d'; lin_dc='--';
color_dc_1=color_bu_1; color_dc_3=color_bu_3; color_dc_4=color_bu_4; sym_dc='v'; lin_dc='-.';
color_bu_2=0.5*(color_bu_1+color_bu_3); color_dc_2=0.5*(color_dc_1+color_dc_3);

%plot
%figure with all waiting times
figure
hold on
p1=errorbar(Qs_buildup_1,mu_lfits_bup_1,dmus_bup_1(1,:).*mu_lfits_bup_1,dmus_bup_1(2,:).*mu_lfits_bup_1,'o','MarkerSize',8,'Color',color_bu_1,'MarkerFaceColor',color_bu_1,'LineWidth',2,'LineStyle', 'none','DisplayName','$\textrm{Re}=76$');
errorbar(Qs_decay_1,mu_lfits_dec_1,dmus_dec_1(1,:).*mu_lfits_dec_1,dmus_dec_1(2,:).*mu_lfits_dec_1,sym_dc,'MarkerSize',8,'Color',color_dc_1,'MarkerFaceColor',color_dc_1,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=76$')
p2=errorbar(Qs_buildup_2,mu_lfits_bup_2,dmus_bup_2(1,:).*mu_lfits_bup_2,dmus_bup_2(2,:).*mu_lfits_bup_2,'o','MarkerSize',8,'Color',color_bu_2,'MarkerFaceColor',color_bu_2,'LineWidth',2,'LineStyle', 'none','DisplayName','$\textrm{Re}=131$');
errorbar(Qs_decay_2,mu_lfits_dec_2,dmus_dec_2(1,:).*mu_lfits_dec_2,dmus_dec_2(2,:).*mu_lfits_dec_2,sym_dc,'MarkerSize',8,'Color',color_dc_2,'MarkerFaceColor',color_dc_2,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=131$')
p3=errorbar(Qs_buildup_3,mu_lfits_bup_3,dmus_bup_3(1,:).*mu_lfits_bup_3,dmus_bup_3(2,:).*mu_lfits_bup_3,'o','MarkerSize',8,'Color',color_bu_3,'MarkerFaceColor',color_bu_3,'LineWidth',2,'LineStyle', 'none','DisplayName','$\textrm{Re}=192$');
errorbar(Qs_decay_3,mu_lfits_dec_3,dmus_dec_3(1,:).*mu_lfits_dec_3,dmus_dec_3(2,:).*mu_lfits_dec_3,sym_dc,'MarkerSize',8,'Color',color_dc_3,'MarkerFaceColor',color_dc_3,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=192$')
p4=errorbar(Qs_buildup_4,mu_lfits_bup_4,dmus_bup_4(1,:).*mu_lfits_bup_4,dmus_bup_4(2,:).*mu_lfits_bup_4,'o','MarkerSize',8,'Color',color_bu_4,'MarkerFaceColor',color_bu_4,'LineWidth',2,'LineStyle', 'none','DisplayName','$\textrm{Re}=329$');
errorbar(Qs_decay_4,mu_lfits_dec_4,dmus_dec_4(1,:).*mu_lfits_dec_4,dmus_dec_4(2,:).*mu_lfits_dec_4,sym_dc,'MarkerSize',8,'Color',color_dc_4,'MarkerFaceColor',color_dc_4,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=329$')
legend([p1,p2,p3,p4],'Location','northeast')
xline(Q0_lfit_bu_1,'--','Color',color_bu_1,'LineWidth',2);
xline(Q0_lfit_bu_2,'--','Color',color_bu_2,'LineWidth',2);
xline(Q0_lfit_bu_3,'--','Color',color_bu_3,'LineWidth',2);
xline(Q0_lfit_bu_4,'--','Color',color_bu_4,'LineWidth',2);
xline(Q0_lfit_dc_1,lin_dc,'Color',color_dc_1,'LineWidth',2);
xline(Q0_lfit_dc_2,lin_dc,'Color',color_dc_2,'LineWidth',2);
xline(Q0_lfit_dc_3,lin_dc,'Color',color_dc_3,'LineWidth',2);
xline(Q0_lfit_dc_4,lin_dc,'Color',color_dc_4,'LineWidth',2);
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\tau_W/\tau_f$')

saveas(gcf,'Fig_factories/JFM/figs/Re_tauw','epsc')

% %get only the legend
% figure
% hold on
% p1=errorbar(Qs_buildup_1,mu_lfits_bup_1,dmus_bup_1(1,:).*mu_lfits_bup_1,dmus_bup_1(2,:).*mu_lfits_bup_1,'o','MarkerSize',8,'Color',color_bu_1,'MarkerFaceColor',color_bu_1,'LineWidth',2,'LineStyle', 'none','DisplayName','build-up $\textrm{Re}=76$');
% p2=errorbar(Qs_decay_1,mu_lfits_dec_1,dmus_dec_1(1,:).*mu_lfits_dec_1,dmus_dec_1(2,:).*mu_lfits_dec_1,sym_dc,'MarkerSize',8,'Color',color_dc_1,'MarkerFaceColor',color_dc_1,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=76$');
% p3=errorbar(Qs_buildup_2,mu_lfits_bup_2,dmus_bup_2(1,:).*mu_lfits_bup_2,dmus_bup_2(2,:).*mu_lfits_bup_2,'o','MarkerSize',8,'Color',color_bu_2,'MarkerFaceColor',color_bu_2,'LineWidth',2,'LineStyle', 'none','DisplayName','build-up $\textrm{Re}=131$');
% p4=errorbar(Qs_decay_2,mu_lfits_dec_2,dmus_dec_2(1,:).*mu_lfits_dec_2,dmus_dec_2(2,:).*mu_lfits_dec_2,sym_dc,'MarkerSize',8,'Color',color_dc_2,'MarkerFaceColor',color_dc_2,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=131$');
% p5=errorbar(Qs_buildup_3,mu_lfits_bup_3,dmus_bup_3(1,:).*mu_lfits_bup_3,dmus_bup_3(2,:).*mu_lfits_bup_3,'o','MarkerSize',8,'Color',color_bu_3,'MarkerFaceColor',color_bu_3,'LineWidth',2,'LineStyle', 'none','DisplayName','build-up $\textrm{Re}=192$');
% p6=errorbar(Qs_decay_3,mu_lfits_dec_3,dmus_dec_3(1,:).*mu_lfits_dec_3,dmus_dec_3(2,:).*mu_lfits_dec_3,sym_dc,'MarkerSize',8,'Color',color_dc_3,'MarkerFaceColor',color_dc_3,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=192$');
% p7=errorbar(Qs_buildup_4,mu_lfits_bup_4,dmus_bup_4(1,:).*mu_lfits_bup_4,dmus_bup_4(2,:).*mu_lfits_bup_4,'o','MarkerSize',8,'Color',color_bu_4,'MarkerFaceColor',color_bu_4,'LineWidth',2,'LineStyle', 'none','DisplayName','build-up $\textrm{Re}=329$');
% p8=errorbar(Qs_decay_4,mu_lfits_dec_4,dmus_dec_4(1,:).*mu_lfits_dec_4,dmus_dec_4(2,:).*mu_lfits_dec_4,sym_dc,'MarkerSize',8,'Color',color_dc_4,'MarkerFaceColor',color_dc_4,'LineWidth',2,'LineStyle', 'none','DisplayName','decay $\textrm{Re}=329$');
% xlim([0,10])
% legend([p1,p3,p5,p7,p2,p4,p6,p8],'NumColumns',2)
% 
% saveas(gcf,'Fig_factories/JFM/figs/Re_tauw_legend','epsc')

%transformed waiting times
figure
hold on
yyaxis right
p1=plot([Q0_lfit_bu_1,Qs_buildup_1(1)],[0,(timescale_1)^powr_bu*(c_lfit_bu_1*Qs_buildup_1(1)-c_lfit_bu_1*Q0_lfit_bu_1)],':','Color',0.75+0.25*color_bu_1,'LineWidth',2);
p2=plot([Q0_lfit_bu_2,Qs_buildup_2(1)],[0,(timescale_2)^powr_bu*(c_lfit_bu_2*Qs_buildup_2(1)-c_lfit_bu_2*Q0_lfit_bu_2)],':','Color',0.75+0.25*color_bu_2,'LineWidth',2);
p3=plot([Q0_lfit_bu_3,Qs_buildup_3(1)],[0,(timescale_3)^powr_bu*(c_lfit_bu_3*Qs_buildup_3(1)-c_lfit_bu_3*Q0_lfit_bu_3)],':','Color',0.75+0.25*color_bu_3,'LineWidth',2);
p4=plot([Q0_lfit_bu_4,Qs_buildup_4(1)],[0,(timescale_4)^powr_bu*(c_lfit_bu_4*Qs_buildup_4(1)-c_lfit_bu_4*Q0_lfit_bu_4)],':','Color',0.75+0.25*color_bu_4,'LineWidth',2);
p1.ZData = -ones(size(p1.XData)); p2.ZData = -ones(size(p2.XData)); p3.ZData = -ones(size(p3.XData)); p4.ZData = -ones(size(p4.XData));
yyaxis left
p1=plot([Q0_lfit_dc_1,Qs_decay_1(end)],[0,(timescale_1)^powr_dc*(c_lfit_dc_1*Qs_decay_1(end)-c_lfit_dc_1*Q0_lfit_dc_1)],':','Color',0.75+0.25*color_dc_1,'LineWidth',2);
p2=plot([Q0_lfit_dc_2,Qs_decay_2(end)],[0,(timescale_2)^powr_dc*(c_lfit_dc_2*Qs_decay_2(end)-c_lfit_dc_2*Q0_lfit_dc_2)],':','Color',0.75+0.25*color_dc_2,'LineWidth',2);
p3=plot([Q0_lfit_dc_3,Qs_decay_3(end)],[0,(timescale_3)^powr_dc*(c_lfit_dc_3*Qs_decay_3(end)-c_lfit_dc_3*Q0_lfit_dc_3)],':','Color',0.75+0.25*color_dc_3,'LineWidth',2);
p4=plot([Q0_lfit_dc_4,Qs_decay_4(end)],[0,(timescale_4)^powr_dc*(c_lfit_dc_4*Qs_decay_4(end)-c_lfit_dc_4*Q0_lfit_dc_4)],':','Color',0.75+0.25*color_dc_4,'LineWidth',2);
p1.ZData = -ones(size(p1.XData)); p2.ZData = -ones(size(p2.XData)); p3.ZData = -ones(size(p3.XData)); p4.ZData = -ones(size(p4.XData));
yyaxis right
errorbar(Qs_buildup_1,(1./mu_lfits_bup_1).^powr_bu,powr_bu*dmus_bup_1(2,:).*(1./mu_lfits_bup_1).^powr_bu,powr_bu*dmus_bup_1(1,:).*(1./mu_lfits_bup_1).^powr_bu,'o','MarkerSize',8,'Color',color_bu_1,'MarkerFaceColor',color_bu_1,'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up');
errorbar(Q0_lfit_bu_1,0,dQ0_lfit_bu_1,'horizontal','x','Color',color_bu_1,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(build-up)}$');
errorbar(Qs_buildup_2,(1./mu_lfits_bup_2).^powr_bu,powr_bu*dmus_bup_2(2,:).*(1./mu_lfits_bup_2).^powr_bu,powr_bu*dmus_bup_2(1,:).*(1./mu_lfits_bup_2).^powr_bu,'o','MarkerSize',8,'Color',color_bu_2,'MarkerFaceColor',color_bu_2,'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up');
errorbar(Q0_lfit_bu_2,0,dQ0_lfit_bu_2,'horizontal','x','Color',color_bu_2,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(build-up)}$');
errorbar(Qs_buildup_3,(1./mu_lfits_bup_3).^powr_bu,powr_bu*dmus_bup_3(2,:).*(1./mu_lfits_bup_3).^powr_bu,powr_bu*dmus_bup_3(1,:).*(1./mu_lfits_bup_3).^powr_bu,'o','MarkerSize',8,'Color',color_bu_3,'MarkerFaceColor',color_bu_3,'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up');
errorbar(Q0_lfit_bu_3,0,dQ0_lfit_bu_3,'horizontal','x','Color',color_bu_3,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(build-up)}$');
errorbar(Qs_buildup_4,(1./mu_lfits_bup_4).^powr_bu,powr_bu*dmus_bup_4(2,:).*(1./mu_lfits_bup_4).^powr_bu,powr_bu*dmus_bup_4(1,:).*(1./mu_lfits_bup_4).^powr_bu,'o','MarkerSize',8,'Color',color_bu_4,'MarkerFaceColor',color_bu_4,'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up');
errorbar(Q0_lfit_bu_4,0,dQ0_lfit_bu_4,'horizontal','x','Color',color_bu_4,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(build-up)}$');
yyaxis left
errorbar(Qs_decay_1,(1./mu_lfits_dec_1).^powr_dc,powr_dc*dmus_dec_1(2,:).*(1./mu_lfits_dec_1).^powr_dc,powr_dc*dmus_dec_1(1,:).*(1./mu_lfits_dec_1).^powr_dc,sym_dc,'MarkerSize',8,'Color',color_dc_1,'MarkerFaceColor',color_dc_1,'LineWidth',2,'LineStyle', 'none','DisplayName','Decay');
errorbar(Q0_lfit_dc_1,0,dQ0_lfit_dc_1,'horizontal','x','Color',color_dc_1,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(decay)}$');
errorbar(Qs_decay_2,(1./mu_lfits_dec_2).^powr_dc,powr_dc*dmus_dec_2(2,:).*(1./mu_lfits_dec_2).^powr_dc,powr_dc*dmus_dec_2(1,:).*(1./mu_lfits_dec_2).^powr_dc,sym_dc,'MarkerSize',8,'Color',color_dc_2,'MarkerFaceColor',color_dc_2,'LineWidth',2,'LineStyle', 'none','DisplayName','Decay');
errorbar(Q0_lfit_dc_2,0,dQ0_lfit_dc_2,'horizontal','x','Color',color_dc_2,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(decay)}$');
errorbar(Qs_decay_3,(1./mu_lfits_dec_3).^powr_dc,powr_dc*dmus_dec_3(2,:).*(1./mu_lfits_dec_3).^powr_dc,powr_dc*dmus_dec_3(1,:).*(1./mu_lfits_dec_3).^powr_dc,sym_dc,'MarkerSize',8,'Color',color_dc_3,'MarkerFaceColor',color_dc_3,'LineWidth',2,'LineStyle', 'none','DisplayName','Decay');
errorbar(Q0_lfit_dc_3,0,dQ0_lfit_dc_3,'horizontal','x','Color',color_dc_3,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(decay)}$');
errorbar(Qs_decay_4,(1./mu_lfits_dec_4).^powr_dc,powr_dc*dmus_dec_4(2,:).*(1./mu_lfits_dec_4).^powr_dc,powr_dc*dmus_dec_4(1,:).*(1./mu_lfits_dec_4).^powr_dc,sym_dc,'MarkerSize',8,'Color',color_dc_4,'MarkerFaceColor',color_dc_4,'LineWidth',2,'LineStyle', 'none','DisplayName','Decay');
errorbar(Q0_lfit_dc_4,0,dQ0_lfit_dc_4,'horizontal','x','Color',color_dc_4,'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(decay)}$');
grid on
box on
yyaxis right
plot(Qs_buildup_1,(timescale_1)^powr_bu*(c_lfit_bu_1*Qs_buildup_1-c_lfit_bu_1*Q0_lfit_bu_1),'--','Color',color_bu_1,'LineWidth',2)
plot(Qs_buildup_2,(timescale_2)^powr_bu*(c_lfit_bu_2*Qs_buildup_2-c_lfit_bu_2*Q0_lfit_bu_2),'--','Color',color_bu_2,'LineWidth',2)
plot(Qs_buildup_3,(timescale_3)^powr_bu*(c_lfit_bu_3*Qs_buildup_3-c_lfit_bu_3*Q0_lfit_bu_3),'--','Color',color_bu_3,'LineWidth',2)
plot(Qs_buildup_4,(timescale_4)^powr_bu*(c_lfit_bu_4*Qs_buildup_4-c_lfit_bu_4*Q0_lfit_bu_4),'--','Color',color_bu_4,'LineWidth',2)
yyaxis left
plot(Qs_decay_1,(timescale_1)^powr_dc*(c_lfit_dc_1*Qs_decay_1-c_lfit_dc_1*Q0_lfit_dc_1),'--','Color',color_dc_1,'LineWidth',2)
plot(Qs_decay_2,(timescale_2)^powr_dc*(c_lfit_dc_2*Qs_decay_2-c_lfit_dc_2*Q0_lfit_dc_2),'--','Color',color_dc_2,'LineWidth',2)
plot(Qs_decay_3,(timescale_3)^powr_dc*(c_lfit_dc_3*Qs_decay_3-c_lfit_dc_3*Q0_lfit_dc_3),'--','Color',color_dc_3,'LineWidth',2)
plot(Qs_decay_4,(timescale_4)^powr_dc*(c_lfit_dc_4*Qs_decay_4-c_lfit_dc_4*Q0_lfit_dc_4),'--','Color',color_dc_4,'LineWidth',2)
xlabel('$Q$')
yyaxis left
ylim([0 inf])
ylabel('$1/(\tau_W^{\mathrm{(decay)}}/\tau_f)^{1/2}$')
yyaxis right
ylim([0 inf])
ylabel('$1/(\tau_W^{\mathrm{(build-up)}}/\tau_f)^{1/3}$')
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';
set(gca, 'SortMethod', 'depth')
set(gcf,'Renderer','painters')
xticks(linspace(0.8,2.2,8))

saveas(gcf,'Fig_factories/JFM/figs/Re_tauw_pow','epsc')

%Q0s
figure
hold on
x=[Re_1,Re_2,Re_3,Re_4];
errorbar(x,[Q0_lfit_bu_1,Q0_lfit_bu_2,Q0_lfit_bu_3,Q0_lfit_bu_4],[dQ0_lfit_bu_1,dQ0_lfit_bu_2,dQ0_lfit_bu_3,dQ0_lfit_bu_4],'s-','MarkerSize',6,'MarkerFaceColor',gPlotColors(2,:),'Color',gPlotColors(2,:),'LineWidth',2,'DisplayName','$Q_0^{\mathrm{(build-up)}}$')
errorbar(x,[Q0_lfit_dc_1,Q0_lfit_dc_2,Q0_lfit_dc_3,Q0_lfit_dc_4],[dQ0_lfit_dc_1,dQ0_lfit_dc_2,dQ0_lfit_dc_3,dQ0_lfit_dc_4],'s-','MarkerSize',6,'MarkerFaceColor',gPlotColors(1,:),'Color',gPlotColors(1,:),'LineWidth',2,'DisplayName','$Q_0^{\mathrm{(decay)}}$')
%legend('Location','southeast')
ylim([0.8,2.0])
Q0s_bu=[Q0_lfit_bu_1,Q0_lfit_bu_2,Q0_lfit_bu_3,Q0_lfit_bu_4]; Q0s_dc=[Q0_lfit_dc_1,Q0_lfit_dc_2,Q0_lfit_dc_3,Q0_lfit_dc_4];
patch([x fliplr(x)], [Q0s_bu max(ylim)*ones(1,length(Q0s_bu))], 0.85*[1,0.9,1])        % Above Q0_bu
patch([x fliplr(x)], [Q0s_dc min(ylim)*ones(1,length(Q0s_dc))], 0.85*[1,0.9,1])        % Below Q0_dc
patch([x fliplr(x)], [Q0s_bu min(ylim)*ones(1,length(Q0s_bu))], 0.4+0.6*gPlotColors(2,:))        % Below Q0_bu
patch([x fliplr(x)], [Q0s_dc max(ylim)*ones(1,length(Q0s_dc))], 0.4+0.6*gPlotColors(1,:))        % Above Q0_dc
[Rec,Qc]=getLogCrossing(Re_3,Q0_lfit_bu_3,Re_4,Q0_lfit_bu_4,Re_3,Q0_lfit_dc_3,Re_4,Q0_lfit_dc_4);
patch([Re_4,Rec,Re_4,Re_4], [Q0_lfit_bu_4,Qc,Q0_lfit_dc_4,Q0_lfit_bu_4], (1/256)*[87,16,137])
cs=get(gca,'Children');
set(gca,'Children',cs([6,7,1,2,3,4,5])) %make sure areas are on background
text(Re_1*1.30,Q0_bu_1*1.80,'Condensate stable','FontSize',18)
text(Re_1*1.32,Q0_bu_1*1.239,'Bimodal','Rotation',30,'FontSize',18) %both metastable
text(Re_1*2.25,Q0_dc_3*0.74,'3D state stable','FontSize',18)
text(Re_1*3.55,Qc*0.960,{'Pure hysteresis','(bistable)'},'Rotation',17,'Color',0.98*[1,1,1],'FontSize',18,'HorizontalAlignment','center')
annotation('textarrow',[0.82,0.86],[0.715,0.777],'Linewidth',2,'Color',0.95*[1,1,1])
ylabel('$Q$')
xlabel('$Re$')
xlim([x(1),x(end)])
grid on
box on
set(gca, 'XScale', 'log')

saveas(gcf,'Fig_factories/JFM/figs/Re_Q0','epsc')

function [Rec,Qc]=getLogCrossing(lRe1,lQ1,rRe1,rQ1,lRe2,lQ2,rRe2,rQ2)
    lRe1=log(lRe1); rRe1=log(rRe1); lRe2=log(lRe2); rRe2=log(rRe2);
    Rec=((-lQ1+lQ2)*rRe1*rRe2+lRe1*rRe2*(-lQ2+rQ1)+lRe2*(lQ1*rRe1-rRe1*rQ2+lRe1*(-rQ1+rQ2)))/((lRe2-rRe2)*(lQ1-rQ1)-(lRe1-rRe1)*(lQ2-rQ2));
    Qc=((rQ1-lQ1)/(rRe1-lRe1))*(Rec-lRe1)+lQ1;
    Rec=exp(Rec);
end
