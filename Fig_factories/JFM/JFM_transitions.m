%buildup
series='F9R192/Buildup/Q150/';
runs=[28,6,12,19,30,7];

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

figure
t=tiledlayout(2,1);
ax1=nexttile;

hold on
ts=getTsFromFile([gDataDir 'examples/run14/energy2D_ls_balance.txt'],7,1,2,false);
ithr=findIn(ts.Data,10.6);
plot(ts.Time(ithr:end)/timescale,ts.Data(ithr:end)/energyscale,'LineWidth',1,'Color',0.75+0.25*gPlotColors(1,:))
plot(ts.Time(1:ithr)/timescale,ts.Data(1:ithr)/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
set(gca,'ColorOrderIndex',2)
for irun=1:length(runs)
    ts=getTsFromFile([gDataDir series 'run' num2str(runs(irun)) '/energy2D_ls_balance.txt'],7,1,2,false);
    plot(ts.Time/timescale,ts.Data/energyscale,'LineWidth',1)
end
plot(xlim,xlim*0+10.6/energyscale,'k--','LineWidth',1)
grid on
box on
%xlabel('$t/\tau_f$')
xticklabels({})
%ylabel('$E_\mathrm{ls}/E_f$')

%p=get(gcf,'Position');
%set(gcf,'Position',[p(1),p(2),p(3),round(0.6*p(4))])

%saveas(gcf,'Fig_factories/JFM/figs/example_buildup','epsc')

%decay
series='F9R192/Decay/Q134/';
runs=[16,13,4,15,10,17];

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

%figure
ax2=nexttile

hold on
ts=getTsFromFile([gDataDir 'examples/run27/energy2D_ls_balance.txt'],7,1,2,false);
ithr=findIn(ts.Data,2.0);
istop=findIn(ts.Time,4400);
plot(ts.Time(ithr:istop)/timescale,ts.Data(ithr:istop)/energyscale,'LineWidth',1,'Color',0.75+0.25*gPlotColors(1,:))
plot(ts.Time(1:ithr)/timescale,ts.Data(1:ithr)/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
set(gca,'ColorOrderIndex',2)
for irun=1:length(runs)
    ts=getTsFromFile([gDataDir series 'run' num2str(runs(irun)) '/energy2D_ls_balance.txt'],7,1,2,false);
    plot(ts.Time/timescale,ts.Data/energyscale,'LineWidth',1)
end
ylim([0,25])
yticks([0,10,20])
xlim([0,8000])
plot(xlim,xlim*0+2.0/energyscale,'k--','LineWidth',1)
grid on
box on
%xlabel('$t/\tau_f$')
%ylabel('$E_\mathrm{ls}/E_f$')

%p=get(gcf,'Position');
%set(gcf,'Position',[p(1),p(2),p(3),round(0.6*p(4))])

linkaxes([ax1,ax2],'x');
l1=xlabel(t,'$t/\tau_f$');
l2=ylabel(t,'$E_\mathrm{ls}/E_f$');
t.TileSpacing = 'compact';
%t.Padding = 'compact';
l1.Interpreter='latex';
l1.FontSize=18;
l2.Interpreter='latex';
l2.FontSize=18;

set(gcf,'Renderer','painters')


%saveas(gcf,'Fig_factories/JFM/figs/example_decay','epsc')
saveas(gcf,'Fig_factories/JFM/figs/example_timeseries','epsc')


adrian_decays;
adrian_buildups;

figure
[~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{8}/timescale,false);
stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.7*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.66$')
hold on
[~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(bu_times{11}/timescale,false);
stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.3+0.7*gPlotColors(2,:),'LineWidth',2,'DisplayName','Build-up $Q=1.75$')
[~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{3}/timescale,false);
stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.3+0.7*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.46$')
[~,~,~,~,~,m_ecdf_x,m_ecdf,~,~]=getDistMuT0(dc_times{7}/timescale,false);
stairs(m_ecdf_x,1-m_ecdf,'-','Color',0.7*gPlotColors(1,:),'LineWidth',2,'DisplayName','Decay $Q=1.50$')
legend('Location','southwest')
[~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{8}/timescale,false);
plot(m_ecdf_x,exp(-(m_ecdf_x-(2/3)*t0_lfit)/(1.2*mu_lfit)),'--','Color',0.7*gPlotColors(2,:),'LineWidth',2)
[~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(bu_times{11}/timescale,false);
plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.3+0.7*gPlotColors(2,:),'LineWidth',2)
[~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{3}/timescale,false);
plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.3+0.7*gPlotColors(1,:),'LineWidth',2)
[~,~,~,mu_lfit,t0_lfit,m_ecdf_x,~,~,~]=getDistMuT0(dc_times{7}/timescale,false);
plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'--','Color',0.7*gPlotColors(1,:),'LineWidth',2)
set(gca, 'YScale', 'log')
xlabel('$t_{\mathrm{b},\mathrm{d}}/\tau_f$')
ylabel('Empirical $1-$CDF($t_{\mathrm{b},\mathrm{d}}$)')
ylim([0,1])
%xlim([0 inf])
xlim([0 3000])
grid on
box on

saveas(gcf,'Fig_factories/JFM/figs/dists','epsc')