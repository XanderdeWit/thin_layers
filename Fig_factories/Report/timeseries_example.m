%buildup
series='F9R192/Buildup/Q150/';
runs=[28,6,12,19,30,7];

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

figure
hold on
ts=getTsFromFile([gDataDir 'examples/run14/energy2D_ls_balance.txt'],7,1,2,false);
ithr=findIn(ts.Data,10.6);
plot(ts.Time(ithr:end)/timescale,ts.Data(ithr:end)/energyscale,'LineWidth',1,'Color',0.75+0.25*gPlotColors(1,:))
plot(ts.Time(1:ithr)/timescale,ts.Data(1:ithr)/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
set(gca,'ColorOrderIndex',2)
for irun=1:length(runs)
    ts=getTsFromFile([gDataDir series 'run' num2str(runs(irun)) '/energy2D_ls_balance.txt'],7,1,2,false);
    plot(ts.Time/timescale,ts.Data/energyscale,'LineWidth',1)
end
plot(xlim,xlim*0+10.6/energyscale,'k--','LineWidth',1)
grid on
box on
xlabel('$t/\tau_f$')
ylabel('$E_\mathrm{ls}/E_f$')

saveas(gcf,'Fig_factories/Report/figs/3_example_buildup','epsc')

%decay
series='F9R192/Decay/Q134/';
runs=[16,13,4,15,10,17];

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

figure
hold on
ts=getTsFromFile([gDataDir 'examples/run27/energy2D_ls_balance.txt'],7,1,2,false);
ithr=findIn(ts.Data,2.0);
istop=findIn(ts.Time,4400);
plot(ts.Time(ithr:istop)/timescale,ts.Data(ithr:istop)/energyscale,'LineWidth',1,'Color',0.75+0.25*gPlotColors(1,:))
plot(ts.Time(1:ithr)/timescale,ts.Data(1:ithr)/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
set(gca,'ColorOrderIndex',2)
for irun=1:length(runs)
    ts=getTsFromFile([gDataDir series 'run' num2str(runs(irun)) '/energy2D_ls_balance.txt'],7,1,2,false);
    plot(ts.Time/timescale,ts.Data/energyscale,'LineWidth',1)
end
ylim([0,30])
xlim([0,8000])
plot(xlim,xlim*0+2.0/energyscale,'k--','LineWidth',1)
grid on
box on
xlabel('$t/\tau_f$')
ylabel('$E_\mathrm{ls}/E_f$')

saveas(gcf,'Fig_factories/Report/figs/3_example_decay','epsc')