tsE=getTsFromFile([gDataDir 'examples/run1ab/energy_balance.txt'],5,1,2,false);
tsEls=getTsFromFile([gDataDir 'examples/run1ab/energy2D_ls_balance.txt'],7,1,2,false);

energyscale=getScale('energy',0.5,2*pi/8.0);
timescale=getScale('time',0.5,2*pi/8.0);

figure
hold on
%plot(tsE.Time,0.5*tsE.Data/energyscale,':','Color','k','LineWidth',1.5,'DisplayName','$E_\mathrm{tot}$')
plot(tsEls.Time/timescale,0.5*tsEls.Data/energyscale,'Color',gPlotColors(2,:),'LineWidth',2,'DisplayName','$E_\mathrm{ls}$')
plot(tsE.Time/timescale,0.5*(tsE.Data-tsEls.Data)/energyscale,'Color',gPlotColors(1,:),'LineWidth',1.6,'DisplayName','$E_\mathrm{tot}-E_\mathrm{ls}$')
legend('Location','northwest')
set(gca,'Children',flip(get(gca,'Children')))
grid on
box on
set(gca, 'XLimSpec', 'Tight');
xlabel('$t/\tau_f$')
ylabel('Kinetic energy (in $E_f$)')

saveas(gcf,'Fig_factories/Report/figs/3_condensate_energy_ts','epsc')