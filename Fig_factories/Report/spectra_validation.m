f_spec1='spectra/K6Q94/kkspectrum_y.120.txt';
f_spec2='spectra/K9Q152/kkspectrum_y.058.txt';
f_spec3='spectra/R76Q82/kkspectrum_y.158.txt';
f_spec4='spectra/R329Q150/kkspectrum_y.040.txt';

file = fopen([gDataDir f_spec1],'r');
spec1 = fscanf(file,'%f %f\n',[3 Inf]);
file = fopen([gDataDir f_spec2],'r');
spec2 = fscanf(file,'%f %f\n',[3 Inf]);
file = fopen([gDataDir f_spec3],'r');
spec3 = fscanf(file,'%f %f\n',[3 Inf]);
file = fopen([gDataDir f_spec4],'r');
spec4 = fscanf(file,'%f %f\n',[3 Inf]);
fclose('all');

energyscale1=getScale('energy',0.5,2*pi/6.0);
energyscale2=getScale('energy',0.5,2*pi/9.0);
energyscale3=getScale('energy',0.5,2*pi/8.0);
energyscale4=getScale('energy',0.5,2*pi/8.0);

k53=10:30;
Ek53=6*k53.^(-5/3);

figure
plot((0:size(spec3,2)-1)/8,sum(spec3,1)/energyscale3,'LineWidth',2,'DisplayName','$1/K=8$, $\textrm{Re}=76$')
hold on
plot((0:size(spec1,2)-1)/6,sum(spec1,1)/energyscale1,'LineWidth',2,'DisplayName','$1/K=6$, $\textrm{Re}=192$')
plot((0:size(spec2,2)-1)/9,sum(spec2,1)/energyscale2,'LineWidth',2,'DisplayName','$1/K=9$, $\textrm{Re}=192$')
plot((0:size(spec4,2)-1)/8,sum(spec4,1)/energyscale4,'LineWidth',2,'DisplayName','$1/K=8$, $\textrm{Re}=329$')
legend
%plot(k53,Ek53,'k')
box on
grid on
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
xlabel('Horizontal wavenumber $k/k_f$')
ylabel('Spectrum $E(k)/E_f$')
%xlim([1 100])

saveas(gcf,'Fig_factories/Report/figs/2_spectra_validation','epsc')