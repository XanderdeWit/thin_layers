adrian_buildups;
adrian_decays;

timescale=getScale('time',0.5,2*pi/8.0);

mu_lfits_bup=mu_lfits_bup/timescale; mu_lfits_dec=mu_lfits_dec/timescale;

figure
hold on
errorbar(Qs_buildup,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up')
errorbar(Qs_decay,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Decay')
xline(Q0_lfit_bu,'--','Color',gPlotColors(2,:),'LineWidth',2,'DisplayName','$Q_0^{\mathrm{(build-up)}}$');
xline(Q0_lfit_dc,'--','Color',gPlotColors(1,:),'LineWidth',2,'DisplayName','$Q_0^{\mathrm{(decay)}}$');
legend
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\tau_W/\tau_f$')

saveas(gcf,'Fig_factories/Report/figs/3_adrian_results_tauw','epsc')

figure
hold on
yyaxis right
p1=errorbar(Qs_buildup,(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mu_lfits_bup).^powr_bu,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up');
%p3=errorbar(Q0_lfit_bu,0,dQ0_lfit_bu,'horizontal','x','Color',gPlotColors(2,:),'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(build-up)}$');
p3=errorbar(Q0_lfit_bu,0,dQ0_lfit_bu,'horizontal','.','Color',gPlotColors(2,:),'LineWidth',4,'DisplayName','$Q_0^{\mathrm{(build-up)}}$');
yyaxis left
p2=errorbar(Qs_decay,(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mu_lfits_dec).^powr_dc,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Decay');
%p4=errorbar(Q0_lfit_dc,0,dQ0_lfit_dc,'horizontal','x','Color',gPlotColors(1,:),'MarkerSize',10,'LineWidth',4,'DisplayName','$Q_0^{(decay)}$');
p4=errorbar(Q0_lfit_dc,0,dQ0_lfit_dc,'horizontal','.','Color',gPlotColors(1,:),'LineWidth',4,'DisplayName','$Q_0^{\mathrm{(decay)}}$');
legend([p1,p2,p3,p4],'Location','southeast')
grid on
box on
yyaxis right
plot(Qs_buildup,(timescale)^powr_bu*(c_lfit_bu*Qs_buildup-c_lfit_bu*Q0_lfit_bu),'--','Color',gPlotColors(2,:),'LineWidth',2)
yyaxis left
plot(Qs_decay,(timescale)^powr_dc*(c_lfit_dc*Qs_decay-c_lfit_dc*Q0_lfit_dc),'--','Color',gPlotColors(1,:),'LineWidth',2)
xlabel('$Q$')
yyaxis left
ylim([0 inf])
ylabel('$1/(\tau_W^{\mathrm{(decay)}}/\tau_f)^{1/2}$')
yyaxis right
ylim([0 inf])
ylabel('$1/(\tau_W^{\mathrm{(build-up)}}/\tau_f)^{1/3}$')
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

saveas(gcf,'Fig_factories/Report/figs/3_adrian_results_tauw_pow','epsc')