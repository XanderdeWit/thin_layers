snap_dir='snaps/Nu1e-3_nxz512_ny16_Qy32/';

snap='hd3Dwy.stitch.003.mat';

%Qy=32;
Qy=12; %use different Qy for visual purposes

load([gDataDir snap_dir snap])

xs=linspace(0,2*pi,size(data,1));
ys=linspace(0,2*pi,size(data,2));
zs=linspace(0,2*pi/Qy,size(data,3));

valueLim=0.5*[-max(abs(data),[],'all'),max(abs(data),[],'all')];

figure
drawSlices(xs,ys,zs,0,0,zs(end),-data,valueLim,gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
xlabel('')
ylabel('')
zlabel('')
box on
hold on
line(xlim,[0,0],[zs(end) zs(end)],'Color','k')
line([0,0],ylim,[zs(end) zs(end)],'Color','k')
line([0,0],[0,0],[0,zs(end)],'Color','k')
colorbar off

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.9*p(4)),p(4)])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/1_condensate_snap','-dpng','-opengl','-r600')