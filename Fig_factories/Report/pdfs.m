%% BIMODAL

all_series={'F8R76/PDFs/Q83/','F8R76/PDFs/Q84/','F8R76/PDFs/Q85/'};
Qs=[8.3,8.4,8.5]/8.0;

energyscale=getScale('energy',0.5,2*pi/8.0);
timescale=getScale('time',0.5,2*pi/8.0);

figure
ts=getTsFromFile([gDataDir 'F8R76/PDFs/Q84/' 'run1' '/energy2D_ls_balance.txt'],7,1,2,true);
plot(ts.Time/timescale-1e4,ts.Data/energyscale,'LineWidth',1,'Color',gPlotColors(1,:))
grid on
box on
xlabel('$t/\tau_f$')
ylabel('$E_\mathrm{ls}/E_f$')
xlim([0e4,2e4])

saveas(gcf,'Fig_factories/Report/figs/6_bimodal_timeseries','epsc')

figure
hold on
%togethers
icol=[2,1,3];
for iser=1:length(all_series)
    [pdfTogether,~]=getBimodalPDF([gDataDir char(all_series(iser))],'ks');
    plot(pdfTogether(1,:)/energyscale,pdfTogether(2,:)*energyscale,'LineWidth',2,'DisplayName',['$Q=' num2str(Qs(iser),3) '$'],'Color',gPlotColors(icol(iser),:))
end
legend
xlabel('$E_\mathrm{ls}/E_f$')
ylabel('PDF($E_\mathrm{ls}$)')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/6_bimodal_pdfs','epsc')

%% PURE HYSTERESIS

energyscale=getScale('energy',0.5,2*pi/9.0);
timescale=getScale('time',0.5,2*pi/9.0);

runL2='K9_PDFs/run142';
runH2='K9_PDFs/run142H';

tsL2=getTsFromFile([gDataDir runL2 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH2=getTsFromFile([gDataDir runH2 '/energy2D_ls_balance.txt'],7,1,2,true);

figure
plot(tsL2.Time/timescale,tsL2.Data/energyscale,'Color',gPlotColors(1,:))
hold on
plot(tsH2.Time/timescale,tsH2.Data/energyscale,'Color',gPlotColors(1,:))
grid on
box on
xlabel('$t/\tau_f$')
ylabel('$E_\mathrm{ls}/E_f$')
xlim([0e4,2e4])

saveas(gcf,'Fig_factories/Report/figs/6_purehyst_timeseries','epsc')

runL1='K9_PDFs/run140';
runH1='K9_PDFs/run140H';

tsL1=getTsFromFile([gDataDir runL1 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH1=getTsFromFile([gDataDir runH1 '/energy2D_ls_balance.txt'],7,1,2,true);

runL3='K9_PDFs/run144';
runH3='K9_PDFs/run144H';

tsL3=getTsFromFile([gDataDir runL3 '/energy2D_ls_balance.txt'],7,1,2,true);
tsH3=getTsFromFile([gDataDir runH3 '/energy2D_ls_balance.txt'],7,1,2,true);

tsLs=[tsL1,tsL2,tsL3]; tsHs=[tsH1,tsH2,tsH3];

figure
hold on
icol=[2,1,3];
Qs=[14.0,14.2,14.4]/9;
for its=1:3
    %calculate PDF
    %hist
    % [epdfL,edges] = histcounts(tsLs(its).Data,50,'Normalization', 'pdf');
    % xiL=0.5*(edges(2:end)+edges(1:end-1));
    % [epdfH,edges] = histcounts(tsHs(its).Data,50,'Normalization', 'pdf');
    % xiH=0.5*(edges(2:end)+edges(1:end-1));
    %ksdensity
    [epdfL,xiL]=ksdensity(tsLs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    [epdfH,xiH]=ksdensity(tsHs(its).Data,'Support','positive','BoundaryCorrection','reflection');

    %plot PDFs
    ps(its)=plot(xiL/energyscale,epdfL*energyscale,'LineWidth',2,'Color',gPlotColors(icol(its),:),'DisplayName',['$Q=' num2str(Qs(its),'%.2f') '$']);
    plot(xiH/energyscale,epdfH*energyscale,'LineWidth',2,'Color',gPlotColors(icol(its),:))
    
    xiHext=linspace(0.95*xiH(1),1.05*xiH(end),100);
    %plot gaussian
    plot(xiHext/energyscale,energyscale*normpdf(xiHext,mean(tsHs(its).Data),std(tsHs(its).Data)),'-.','LineWidth',2,'Color',gPlotColors(icol(its),:))
end
legend(ps,'Location','southeast')
cs=get(gca,'Children');
set(gca,'Children',cs([4,5,6,1,2,3,7,8,9]));
xlabel('$E_{ls}/E_f$')
ylabel('PDF($E_{ls}$)')
grid on
box on
set(gca, 'YScale', 'log')
ylim([10^-6,2*10^0])

saveas(gcf,'Fig_factories/Report/figs/6_purehyst_pdfs','epsc')

runLK10='K9_PDFs/run1578K10';
runHK10='K9_PDFs/run1578K10H';

energyscaleK10=getScale('energy',0.5,2*pi/10.0);

tsLK10=getTsFromFile([gDataDir runLK10 '/energy2D_ls_balance.txt'],7,1,2,true);
tsHK10=getTsFromFile([gDataDir runHK10 '/energy2D_ls_balance.txt'],7,1,2,true);

tsLs=[tsL2,tsLK10]; tsHs=[tsH2,tsHK10];
Ks=[9,10];
Es=[energyscale,energyscaleK10];
lines={'-','--'};
icol=[1,4];
figure
hold on
for its=1:2
    %calculate PDF
    %hist
    % [epdfL,edges] = histcounts(tsLs(its).Data,50,'Normalization', 'pdf');
    % xiL=0.5*(edges(2:end)+edges(1:end-1));
    % [epdfH,edges] = histcounts(tsHs(its).Data,50,'Normalization', 'pdf');
    % xiH=0.5*(edges(2:end)+edges(1:end-1));
    %ksdensity
    [epdfL,xiL]=ksdensity(tsLs(its).Data,'Support','positive','BoundaryCorrection','reflection');
    [epdfH,xiH]=ksdensity(tsHs(its).Data,'Support','positive','BoundaryCorrection','reflection');

    %plot PDFs
    p2s(its)=plot(xiL/Es(its),epdfL*Es(its),char(lines{its}),'LineWidth',2,'Color',gPlotColors(icol(its),:),'DisplayName',['$1/K=' num2str(Ks(its)) '$']);
    plot(xiH/Es(its),epdfH*Es(its),char(lines{its}),'LineWidth',2,'Color',gPlotColors(icol(its),:))
end
legend(p2s,'Location','southeast')
xlabel('$E_{ls}/E_f$')
ylabel('PDF($E_{ls}$)')
grid on
box on
set(gca, 'YScale', 'log')
ylim([10^-6,2*10^0])

saveas(gcf,'Fig_factories/Report/figs/6_purehyst_pdfs_K','epsc')