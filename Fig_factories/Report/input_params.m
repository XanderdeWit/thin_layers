Ks=[6,7,8,9]; Re=192;
Res=[76,131,192,329]; K=8;

figure
scatter(Ks,Re*ones(size(Ks)),300,gPlotColors(1,:),'x','LineWidth',4)
hold on
scatter(K*ones(size(Res)),Res,300,gPlotColors(1,:),'x','LineWidth',4)
scatter(K,Re,300,gPlotColors(2,:),'x','LineWidth',4)
xlabel('$1/K$')
ylabel('$\textrm{Re}$')
set(gca, 'YScale', 'log')
xlim([5.75,9.25])
ylim([60,390])
xticks([6,7,8,9])
box on
grid on
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(0.9*p(3)),round(0.9*p(4))])
set(gcf,'Renderer','painters')

saveas(gcf,'Fig_factories/Report/figs/2_input_params','epsc')