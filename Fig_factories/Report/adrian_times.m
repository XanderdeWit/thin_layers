file = fopen([gDataDir '/adrian/adrian_buildup.txt'],'r');
data_bu = fscanf(file,'%f %f\n',[3 Inf]);

file = fopen([gDataDir '/adrian/adrian_decay.txt'],'r');
data_dc = fscanf(file,'%f %f\n',[3 Inf]);

timescale=getScale('time',0.5,2*pi/8.0);

data_bu(2:end,:)=data_bu(2:end,:)/timescale;
data_dc(2:end,:)=data_dc(2:end,:)/timescale;

figure
hold on
errorbar(data_bu(1,:)/8,data_bu(2,:),data_bu(3,:),data_bu(3,:),'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Build-up')
errorbar(data_dc(1,:)/8,data_dc(2,:),data_dc(3,:),data_dc(3,:),'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Decay')
legend
plot([1.45,1.7],7*10^3*[1,1],'k--','LineWidth',2)
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\textrm{mean}(t_{\mathrm{b},\mathrm{d}})/\tau_f$')

saveas(gcf,'Fig_factories/Report/figs/3_adrian_times','epsc')