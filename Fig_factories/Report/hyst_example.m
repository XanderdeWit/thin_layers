file = fopen([gDataDir 'adrian/data_lower.txt'],'r');
data_lower = fscanf(file,'%f %f\n',[2 Inf]);
file = fopen([gDataDir 'adrian/data_upper.txt'],'r');
data_upper = fscanf(file,'%f %f\n',[2 Inf]);

energyscale=getScale('energy',0.5,2*pi/8.0);

figure
plot(data_upper(1,:)/8.0,0.5*data_upper(2,:)/energyscale,'s--','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'DisplayName','Upper branch')
hold on
plot(data_lower(1,:)/8.0,0.5*data_lower(2,:)/energyscale,'d--','MarkerSize',8,'LineWidth',1.5,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'DisplayName','Lower branch')
legend('Location','northwest')
set(gca,'Children',fftshift(get(gca,'Children')))
grid on
box on
xlabel('$Q$')
ylabel('$E_\mathrm{ls}/E_f$')
xlim([1.5,1.68])

saveas(gcf,'Fig_factories/Report/figs/3_hyst_example','epsc')