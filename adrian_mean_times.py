import numpy as np
import matplotlib.pyplot as plt

buildup=np.loadtxt('adrian_buildup.txt')
decay=np.loadtxt('adrian_decay.txt')

plt.figure()
plt.errorbar(buildup[:,0]/8,buildup[:,1],yerr=buildup[:,2],fmt='ro')
plt.errorbar(decay[:,0]/8,decay[:,1],yerr=decay[:,2],fmt='bd')
plt.yscale('log')
plt.ylim((8*10**1,10**6))
plt.grid()
plt.show()

#inverted times
plt.figure()
plt.errorbar(buildup[:,0]/8,1/buildup[:,1],yerr=(buildup[:,2]/buildup[:,1]**2),fmt='ro')
plt.errorbar(decay[:,0]/8,1/decay[:,1],yerr=(decay[:,2]/decay[:,1]**2),fmt='bd')
plt.grid()
plt.show()

#inverted times without t0
t0=400
plt.figure()
plt.errorbar(buildup[:-3,0]/8,1/(buildup[:-3,1]-400),yerr=(buildup[:-3,2]/buildup[:-3,1]**2),fmt='ro')
plt.grid()
plt.show()