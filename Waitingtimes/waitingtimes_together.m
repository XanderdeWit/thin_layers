% all_series_buildup=all_series_buildup_F9R192;
% Qs_buildup=Qs_buildup_F9R192;
% all_series_decay=all_series_decay_F9R192;
% Qs_decay=Qs_decay_F9R192;

% all_series_buildup=all_series_buildup_F8R329;
% Qs_buildup=Qs_buildup_F8R329;
% all_series_decay=all_series_decay_F8R329;
% Qs_decay=Qs_decay_F8R329;

% all_series_buildup=all_series_buildup_F6R192;
% Qs_buildup=Qs_buildup_F6R192;
% all_series_decay=all_series_decay_F6R192;
% Qs_decay=Qs_decay_F6R192;

% all_series_buildup=all_series_buildup_F7R192;
% Qs_buildup=Qs_buildup_F7R192;
% all_series_decay=all_series_decay_F7R192;
% Qs_decay=Qs_decay_F7R192;

% all_series_buildup=all_series_buildup_F8R76;
% Qs_buildup=Qs_buildup_F8R76;
% all_series_decay=all_series_decay_F8R76;
% Qs_decay=Qs_decay_F8R76;

all_series_buildup=all_series_buildup_F8R130;
Qs_buildup=Qs_buildup_F8R130;
all_series_decay=all_series_decay_F8R130;
Qs_decay=Qs_decay_F8R130;

%buildup and decay
mus_bup=zeros(1,length(all_series_buildup)); mus_dec=zeros(1,length(all_series_decay));
dmus_bup=zeros(2,length(all_series_buildup)); dmus_dec=zeros(2,length(all_series_decay));
t0s_bup=zeros(1,length(all_series_buildup)); t0s_dec=zeros(1,length(all_series_decay));
mu_lfits_bup=zeros(1,length(all_series_buildup)); mu_lfits_dec=zeros(1,length(all_series_decay));
t0_lfits_bup=zeros(1,length(all_series_buildup)); t0_lfits_dec=zeros(1,length(all_series_decay));
for iser=1:length(all_series_buildup)
    buildupTimes=getWaitingtimes([gDataDir char(all_series_buildup(iser))],true);
    [mus_bup(iser),dmus_bup(:,iser),t0s_bup(iser),mu_lfits_bup(iser),t0_lfits_bup(iser),~,~,~,~]=getDistMuT0(buildupTimes,false);
end
for iser=1:length(all_series_decay)
    decayTimes=getWaitingtimes([gDataDir char(all_series_decay(iser))],false);
    [mus_dec(iser),dmus_dec(:,iser),t0s_dec(iser),mu_lfits_dec(iser),t0_lfits_dec(iser),~,~,~,~]=getDistMuT0(decayTimes,false);
end

[Q0_bu,c_bu,powr_bu,dQ0_bu]=getWTfit(Qs_buildup,mus_bup,dmus_bup,true);
[Q0_lfit_bu,c_lfit_bu,~,dQ0_lfit_bu]=getWTfit(Qs_buildup,mu_lfits_bup,dmus_bup,true);
[Q0_dc,c_dc,powr_dc,dQ0_dc]=getWTfit(Qs_decay,mus_dec,dmus_dec,false);
[Q0_lfit_dc,c_lfit_dc,~,dQ0_lfit_dc]=getWTfit(Qs_decay,mu_lfits_dec,dmus_dec,false);

figure
hold on
errorbar(Qs_buildup,mus_bup,dmus_bup(1,:).*mus_bup,dmus_bup(2,:).*mus_bup,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
errorbar(Qs_decay,mus_dec,dmus_dec(1,:).*mus_dec,dmus_dec(2,:).*mus_dec,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
errorbar(Qs_buildup,mu_lfits_bup,dmus_bup(1,:).*mu_lfits_bup,dmus_bup(2,:).*mu_lfits_bup,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
errorbar(Qs_decay,mu_lfits_dec,dmus_dec(1,:).*mu_lfits_dec,dmus_dec(2,:).*mu_lfits_dec,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
xline(Q0_lfit_bu,'--','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2);
xline(Q0_lfit_dc,'--','Color',0.75+0.25*gPlotColors(1,:),'LineWidth',2);
xline(Q0_bu,'--','Color',gPlotColors(2,:),'LineWidth',2);
xline(Q0_dc,'--','Color',gPlotColors(1,:),'LineWidth',2);
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\mu(\tau)$')

figure
hold on
yyaxis right
errorbar(Qs_buildup,(1./mus_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mus_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mus_bup).^powr_bu,'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
yyaxis left
errorbar(Qs_decay,(1./mus_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mus_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mus_dec).^powr_dc,'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
yyaxis right
errorbar(Qs_buildup,(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(2,:).*(1./mu_lfits_bup).^powr_bu,powr_bu*dmus_bup(1,:).*(1./mu_lfits_bup).^powr_bu,'o','MarkerSize',8,'Color',0.75+0.25*gPlotColors(2,:),'MarkerFaceColor',0.75+0.25*gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit build-up')
yyaxis left
errorbar(Qs_decay,(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(2,:).*(1./mu_lfits_dec).^powr_dc,powr_dc*dmus_dec(1,:).*(1./mu_lfits_dec).^powr_dc,'d','MarkerSize',8,'Color',0.75+0.25*gPlotColors(1,:),'MarkerFaceColor',0.75+0.25*gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','Lfit decay')
legend
yyaxis right; set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
yyaxis left; set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
yyaxis right
plot(Qs_buildup,c_lfit_bu*Qs_buildup-c_lfit_bu*Q0_lfit_bu,'--','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2)
plot(Qs_buildup,c_bu*Qs_buildup-c_bu*Q0_bu,'--','Color',gPlotColors(2,:),'LineWidth',2)
errorbar(Q0_lfit_bu,0,dQ0_lfit_bu,'horizontal','x','Color',0.75+0.25*gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
errorbar(Q0_bu,0,dQ0_bu,'horizontal','x','Color',gPlotColors(2,:),'MarkerSize',10,'LineWidth',4)
yyaxis left
plot(Qs_decay,c_lfit_dc*Qs_decay-c_lfit_dc*Q0_lfit_dc,'--','Color',0.75+0.25*gPlotColors(1,:),'LineWidth',2)
plot(Qs_decay,c_dc*Qs_decay-c_dc*Q0_dc,'--','Color',gPlotColors(1,:),'LineWidth',2)
errorbar(Q0_lfit_dc,0,dQ0_lfit_dc,'horizontal','x','Color',0.75+0.25*gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
errorbar(Q0_dc,0,dQ0_dc,'horizontal','x','Color',gPlotColors(1,:),'MarkerSize',10,'LineWidth',4)
xlabel('$Q$')
yyaxis left
ylim([0 inf])
ylabel('$1/\mu^{1/2}(\tau)$')
yyaxis right
ylim([0 inf])
ylabel('$1/\mu^{1/3}(\tau)$')
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

figure
hold on
errorbar(Qs_buildup,mus_bup+t0s_bup,dmus_bup(1,:).*(mus_bup+t0s_bup),dmus_bup(2,:).*(mus_bup+t0s_bup),'o','MarkerSize',8,'Color',gPlotColors(2,:),'MarkerFaceColor',gPlotColors(2,:),'LineWidth',2,'LineStyle', 'none','DisplayName','build-up')
errorbar(Qs_decay,mus_dec+t0s_dec,dmus_dec(1,:).*(mus_dec+t0s_dec),dmus_dec(2,:).*(mus_dec+t0s_dec),'d','MarkerSize',8,'Color',gPlotColors(1,:),'MarkerFaceColor',gPlotColors(1,:),'LineWidth',2,'LineStyle', 'none','DisplayName','decay')
legend
grid on
box on
set(gca, 'YScale', 'log')
xlabel('$Q$')
ylabel('$\textrm{mean}(\tau)$')

figure
hold on
scatter(Qs_buildup,t0s_bup,80,gPlotColors(2,:),'filled','o','DisplayName','build-up')
scatter(Qs_decay,t0s_dec,80,gPlotColors(1,:),'filled','d','DisplayName','decay')
scatter(Qs_buildup,t0_lfits_bup,80,0.75+0.25*gPlotColors(2,:),'filled','o','DisplayName','Lfit build-up')
scatter(Qs_decay,t0_lfits_dec,80,0.75+0.25*gPlotColors(1,:),'filled','d','DisplayName','Lfit decay')
legend
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
grid on
box on
xlabel('$Q$')
ylabel('$\tau_0$')