function plotEventTS(seriespath,buildup,subsample,chained)

dirs=dir(seriespath);
dirs=dirs(3:end);

nsims=length(dirs);

figure
hold on
nplot=0;
time_prev=0;
for icase=1:nsims
    ts=getTsFromFile([seriespath dirs(icase).name '/energy2D_ls_balance.txt'],7,1,2,false);
    if buildup
    	[~,eventtimes]=getWaitingtimeFromFile([seriespath dirs(icase).name '/BuildupTime.txt']);
    else
        [~,eventtimes]=getWaitingtimeFromFile([seriespath dirs(icase).name '/DecayTime.txt']);
    end
    it_prev=1;
    for i_et=1:length(eventtimes)
        it=findIn(ts.Time,eventtimes(i_et));
        if abs(ts.Data(it-1)-ts.Data(it))>2
            it=it-1;
        end
        if mod(nplot,subsample)==0
            if chained
                plot(ts.Time(it_prev+1:it)-ts.Time(it_prev+1)+time_prev,ts.Data(it_prev+1:it))
            else
                plot(ts.Time(it_prev+1:it)-ts.Time(it_prev+1),ts.Data(it_prev+1:it))
            end
            time_prev=time_prev+ts.Time(it)-ts.Time(it_prev+1);
        end
        nplot=nplot+1;
        it_prev=it;
    end
end
grid on
box on
set(gca, 'XLimSpec', 'Tight');
xlabel('$t$')
ylabel('$E_{ls}$')

end

