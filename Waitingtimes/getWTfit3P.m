function [Q0,c,powr] = getWTfit3P(Qs,mus,c_s,Q0_s,pow_s)

[m_fit,~] = fit(Qs',mus',fittype('1/((c*(x-Q0))^(1/p))'),'Lower',0.5*[c_s,Q0_s,pow_s],'Upper',1.5*[c_s,Q0_s,pow_s],'StartPoint',[c_s,Q0_s,pow_s]);

figure
plot(m_fit,Qs',mus')
set(gca, 'YScale', 'log')

c=m_fit.c;
Q0=m_fit.Q0;
powr=m_fit.p;

end

