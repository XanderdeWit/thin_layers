function [mu,dmu,t0,mu_lfit,t0_lfit,m_ecdf_x,m_ecdf,m_ecdf_lo,m_ecdf_up] = getDistMuT0(waiting_times,plotDist,gPlotColors)

waiting_times=sort(waiting_times);

[m_ecdf,m_ecdf_x,m_ecdf_lo,m_ecdf_up]=ecdf(waiting_times);

%dfit_x=m_ecdf_x(1:end-1);
%dfit_y=m_ecdf(1:end-1);
%dfit_x=0.5*(m_ecdf_x(2:end)+m_ecdf_x(1:end-1));
%dfit_y=0.5*(m_ecdf(2:end)+m_ecdf(1:end-1));
dfit_x=0.5*(m_ecdf_x(3:end)+m_ecdf_x(2:end-1));
dfit_y=m_ecdf(2:end-1);

[m_fit,~] = fit(dfit_x,dfit_y,fittype('1-exp(-(x-t0)/(mean-t0))','problem','mean'),'problem',mean(waiting_times),'StartPoint',waiting_times(1));
t0=m_fit.t0;
mu=mean(waiting_times)-t0;
p_lfit=polyfit(dfit_x,log(1-dfit_y),1);
mu_lfit=-1/p_lfit(1);
t0_lfit=-p_lfit(2)/p_lfit(1);

if length(waiting_times)>45 %use 95% CI for Adrians results
    [rel_dn,rel_up]=getExpCI(length(waiting_times),0.05);
    dmu=[rel_dn,rel_up];
else %use 67% CI for my results
    [rel_dn,rel_up]=getExpCI(length(waiting_times),0.33);
    dmu=[rel_dn,rel_up];
end

if plotDist
    figure
    stairs(m_ecdf_x,m_ecdf,'-','Color',gPlotColors(1,:),'LineWidth',2)
    hold on
    stairs(m_ecdf_x,[m_ecdf_lo, m_ecdf_up],':','Color',gPlotColors(1,:),'LineWidth',1.5)
    plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0)/mu),'--','Color',gPlotColors(2,:),'LineWidth',2.5)
    plot(m_ecdf_x,1-exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'-.','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2.5)
    %plot(dfit_x,dfit_y,'r.','MarkerSize',10)
    xlabel('Waiting time')
    ylabel('ECDF')
    ylim([0,1])
    xlim([0 inf])
    grid on
    box on

    figure
    stairs(m_ecdf_x,1-m_ecdf,'-','Color',gPlotColors(1,:),'LineWidth',2)
    hold on
    stairs(m_ecdf_x,[1-m_ecdf_lo, 1-m_ecdf_up],':','Color',gPlotColors(1,:),'LineWidth',1.5)
    plot(m_ecdf_x,exp(-(m_ecdf_x-t0)/mu),'--','Color',gPlotColors(2,:),'LineWidth',2.5)
    plot(m_ecdf_x,exp(-(m_ecdf_x-t0_lfit)/mu_lfit),'-.','Color',0.75+0.25*gPlotColors(2,:),'LineWidth',2.5)
    %plot(dfit_x,1-dfit_y,'r.','MarkerSize',10)
    xlabel('Waiting time')
    ylabel('EPDF')
    set(gca, 'YScale', 'log')
    ylim([exp(-m_ecdf_x(end)/mu),1])
    xlim([0 inf])
    grid on
    box on
end

end

function [rel_dn,rel_up]=getExpCI(n,alpha)

rel_dn=-(2*n/chi2inv(1-alpha/2,2*n)-1);
rel_up=2*n/chi2inv(alpha/2,2*n)-1;

end