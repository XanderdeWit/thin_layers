function [waitingtimes,eventtimes] = getWaitingtimeFromFile(path)

file = fopen(path,'r');
data = fscanf(file,'%f %f\n',[2 Inf]);
fclose('all');

eventtimes=data(2,:);
waitingtimes=diff([0,eventtimes]);

end

