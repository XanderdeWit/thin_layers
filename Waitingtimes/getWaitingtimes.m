function waitingtimes = getWaitingtimes(seriespath,buildup)

dirs=dir(seriespath);
dirs=dirs(3:end);

nsims=length(dirs);

waitingtimes=[];

for icase=1:nsims
    if buildup
    	[iwaitingtimes,~]=getWaitingtimeFromFile([seriespath dirs(icase).name '/BuildupTime.txt']);
    else
        [iwaitingtimes,~]=getWaitingtimeFromFile([seriespath dirs(icase).name '/DecayTime.txt']);
    end
    waitingtimes=[waitingtimes,iwaitingtimes];
end


end

