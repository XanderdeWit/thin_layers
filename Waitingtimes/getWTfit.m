function [Q0,c,powr,dQ0] = getWTfit(Qs,mus,dmus,buildup)

if buildup
    powr=1/3;
else
    powr=1/2;
end

%p_fit=polyfit(Qs,(1./mus).^powr,1);
%Q0=-p_fit(2)/p_fit(1);
%c=p_fit(1);

%flm=fitlm(Qs,(1./mus).^powr);
ws=1./(powr*(0.5*(dmus(1,:)+dmus(2,:))).*(1./mus).^powr).^2;
flm=fitlm(Qs,(1./mus).^powr,'Weights',ws);
b=flm.Coefficients{'(Intercept)','Estimate'}; a=flm.Coefficients{'x1','Estimate'};
Q0=-b/a;
c=a;

vara=flm.CoefficientCovariance(2,2); varb=flm.CoefficientCovariance(1,1); covarab=flm.CoefficientCovariance(1,2);
dQ0=sqrt((b/a^2)^2*vara+(-1/a)^2*varb+2*(-1/a)*(b/a^2)*covarab);
dQ0=1.96*dQ0; % 95%-CI

end

