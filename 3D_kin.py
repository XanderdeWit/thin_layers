# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

data_vx=np.load('data/hd3Dvx_r9.stitch.005.npy')
data_vz=np.load('data/hd3Dvz_r9.stitch.005.npy')

#2D kinetic energy
E_2D=np.mean(data_vx,axis=2)**2+np.mean(data_vz,axis=2)**2

lim=0.6*np.amax(E_2D)

plt.figure()
plt.pcolor(E_2D, shading='auto',vmin=0,vmax=lim)
plt.colorbar()
plt.xlabel('$x$')
plt.ylabel('$y$')


