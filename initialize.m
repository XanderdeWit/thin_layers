%NOTE: this script must be run before running any other scripts
%In this file, all global variables are set.

%The directory of the data
gDataDir='/Volumes/T7 Touch/ENS_internship/data/';

%Set typeface
set(0,'defaultTextInterpreter','latex')
set(0,'defaultLegendInterpreter','latex')
set(0,'defaultAxesTickLabelInterpreter','latex')

set(0,'DefaultAxesFontSize',18)

set(0,'DefaultLegendAutoUpdate','off')

%Set global color map
gMyCM = getRudieColorMap();

gPlotColors=[0 0.4470 0.7410 ; 0.8500 0.3250 0.0980 ; 0.9290 0.6940 0.1250 ; 0.4940 0.1840 0.5560 ; 0.4660 0.6740 0.1880 ; 0.3010 0.7450 0.9330 ; 0.6350 0.0780 0.1840];

%set data series
all_series_buildup_F8R76={'F8R76/Buildup/Q82/','F8R76/Buildup/Q84/','F8R76/Buildup/Q86/','F8R76/Buildup/Q88/','F8R76/Buildup/Q90/'};
Qs_buildup_F8R76=[8.2,8.4,8.6,8.8,9.0]/8.0;
all_series_decay_F8R76={'F8R76/Decay/Q80/','F8R76/Decay/Q82/','F8R76/Decay/Q84/','F8R76/Decay/Q85/','F8R76/Decay/Q86/'};
Qs_decay_F8R76=[8.0,8.2,8.4,8.5,8.6]/8.0;

all_series_buildup_F8R130={'F8R130/Buildup/Q110/','F8R130/Buildup/Q112/','F8R130/Buildup/Q114/','F8R130/Buildup/Q116/','F8R130/Buildup/Q118/'};
Qs_buildup_F8R130=[11.0,11.2,11.4,11.6,11.8]/8.0;
all_series_decay_F8R130={'F8R130/Decay/Q103/','F8R130/Decay/Q104/','F8R130/Decay/Q105/','F8R130/Decay/Q106/','F8R130/Decay/Q107/'};
Qs_decay_F8R130=[10.3,10.4,10.5,10.6,10.7]/8.0;

all_series_buildup_F6R192={'F6R192/Buildup/Q94/','F6R192/Buildup/Q96/','F6R192/Buildup/Q98/','F6R192/Buildup/Q100/','F6R192/Buildup/Q102/'};
Qs_buildup_F6R192=[9.4,9.6,9.8,10.0,10.2]/6.0;
all_series_decay_F6R192={'F6R192/Decay/Q92/','F6R192/Decay/Q94/','F6R192/Decay/Q95/','F6R192/Decay/Q96/','F6R192/Decay/Q98/'};
Qs_decay_F6R192=[9.2,9.4,9.5,9.6,9.8]/6.0;

all_series_buildup_F7R192={'F7R192/Buildup/Q110/','F7R192/Buildup/Q112/','F7R192/Buildup/Q114/','F7R192/Buildup/Q116/','F7R192/Buildup/Q118/'};
Qs_buildup_F7R192=[11.0,11.2,11.4,11.6,11.8]/7.0;
all_series_decay_F7R192={'F7R192/Decay/Q107/','F7R192/Decay/Q108/','F7R192/Decay/Q109/','F7R192/Decay/Q110/','F7R192/Decay/Q111/'};
Qs_decay_F7R192=[10.7,10.8,10.9,11.0,11.1]/7.0;

all_series_buildup_F8R329={'F8R329/Buildup/Q154/','F8R329/Buildup/Q156/','F8R329/Buildup/Q158/','F8R329/Buildup/Q160/','F8R329/Buildup/Q162/','F8R329/Buildup/Q164/'};
Qs_buildup_F8R329=[15.4,15.6,15.8,16.0,16.2,16.4]/8.0;
all_series_decay_F8R329={'F8R329/Decay/Q139/','F8R329/Decay/Q140/','F8R329/Decay/Q141/','F8R329/Decay/Q142/','F8R329/Decay/Q1425/'};
Qs_decay_F8R329=[13.9,14.0,14.1,14.2,14.25]/8.0;

all_series_buildup_F9R192={'F9R192/Buildup/Q150/','F9R192/Buildup/Q151/','F9R192/Buildup/Q152/','F9R192/Buildup/Q154/','F9R192/Buildup/Q156/'};
Qs_buildup_F9R192=[15.0,15.1,15.2,15.4,15.6]/9.0;
all_series_decay_F9R192={'F9R192/Decay/Q130/','F9R192/Decay/Q132/','F9R192/Decay/Q133/','F9R192/Decay/Q134/','F9R192/Decay/Q1345/'};
Qs_decay_F9R192=[13.0,13.2,13.3,13.4,13.45]/9.0;

%configure path
addpath('Adrian')
addpath('Bimodal')
addpath('Fig_factories/JFM')
addpath('Fig_factories/Presentation')
addpath('Fig_factories/Report')
addpath('Snaps')
addpath('Waitingtimes')

function cm=getRudieColorMap()

    RdBu_basic=flipud([103,   0,  31;
                       178,  24,  43;
                       214,  96,  77;
                       244, 165, 130;
                       253, 219, 199;
                       247, 247, 247;
                       209, 229, 240;
                       146, 197, 222;
                        67, 147, 195;
                        33, 102, 172;
                         5,  48,  97])/255;
    x_RdBu_basic=[0; 0.14; 0.23; 0.32; 0.41; 0.5; 0.59; 0.68; 0.77; 0.86; 1];
    x_RdBu=(0:1:63)'/63;
    RdBu=zeros(64,3);
    for k=1:3
      RdBu(:,k)=interp1(x_RdBu_basic,RdBu_basic(:,k),x_RdBu);
    end
    cm=RdBu;

end
