import numpy as np
import matplotlib.pyplot as plt

from matplotlib.widgets import Slider

buildup=np.loadtxt('adrian_buildup.txt')
decay=np.loadtxt('adrian_decay.txt')

plt.figure()
plt.errorbar(buildup[:,0]/8,buildup[:,1],yerr=buildup[:,2],fmt='ro')
plt.errorbar(decay[:,0]/8,decay[:,1],yerr=decay[:,2],fmt='bd')
plt.yscale('log')
plt.ylim((8*10**1,10**6))
plt.grid()
plt.show()


#plot build-up with slider
t00=400
fig, ax = plt.subplots()
datas = plt.errorbar(buildup[:,0]/8,buildup[:,1]-t00,yerr=buildup[:,2],fmt='ro')
fit, = plt.plot(buildup[:,0]/8,buildup[:,1],'r--') #dummy line, updated at update
plt.yscale('log')
plt.grid()

ax.margins(x=0)
plt.subplots_adjust(bottom=0.25)

txt = plt.text(0.6,1.05,'$t_0=%5.3f$' % t00, transform=ax.transAxes)

axslid = plt.axes([0.15, 0.08, 0.65, 0.03], facecolor='lightgoldenrodyellow')
slider = Slider(
    ax=axslid,
    label='$t_0$',
    valmin=0,
    valmax=500,
    valinit=t00,
)

def update_errorbar(errobj, x, y, xerr=None, yerr=None):
    ln, caps, bars = errobj


    if len(bars) == 2:
        assert xerr is not None and yerr is not None, "Your errorbar object has 2 dimension of error bars defined. You must provide xerr and yerr."
        barsx, barsy = bars  # bars always exist (?)
        try:  # caps are optional
            errx_top, errx_bot, erry_top, erry_bot = caps
        except ValueError:  # in case there is no caps
            pass

    elif len(bars) == 1:
        assert (xerr is     None and yerr is not None) or\
               (xerr is not None and yerr is     None),  \
               "Your errorbar object has 1 dimension of error bars defined. You must provide xerr or yerr."

        if xerr is not None:
            barsx, = bars  # bars always exist (?)
            try:
                errx_top, errx_bot = caps
            except ValueError:  # in case there is no caps
                pass
        else:
            barsy, = bars  # bars always exist (?)
            try:
                erry_top, erry_bot = caps
            except ValueError:  # in case there is no caps
                pass

    ln.set_data(x,y)

    try:
        errx_top.set_xdata(x + xerr)
        errx_bot.set_xdata(x - xerr)
        errx_top.set_ydata(y)
        errx_bot.set_ydata(y)
    except NameError:
        pass
    try:
        barsx.set_segments([np.array([[xt, y], [xb, y]]) for xt, xb, y in zip(x + xerr, x - xerr, y)])
    except NameError:
        pass

    try:
        erry_top.set_xdata(x)
        erry_bot.set_xdata(x)
        erry_top.set_ydata(y + yerr)
        erry_bot.set_ydata(y - yerr)
    except NameError:
        pass
    try:
        barsy.set_segments([np.array([[x, yt], [x, yb]]) for x, yt, yb in zip(x, y + yerr, y - yerr)])
    except NameError:
        pass

def update(val):
    txt.set_text('$t_0=%5.3f$' % val)
    update_errorbar(datas, buildup[:,0]/8, buildup[:,1]-val,yerr=buildup[:,2])
    plin = np.polyfit(buildup[:-3,0]/8,np.log(buildup[:-3,1]-val),1)
    fit.set_data(buildup[:-3,0]/8,np.exp(plin[1]+plin[0]*buildup[:-3,0]/8))
    fig.canvas.draw_idle()

slider.on_changed(update)

update(t00)
plt.show()